import { packingListConstants } from '../constants';

const initialState = {
	errorMessage: '',
	orderInfo: [],
	packingList: [],
	packingListItem: [],
	responseOrderPackNew: [],
	sessionId: null,
	finishResponse: null,
	deleteResponse: null,
	dataPrint: []
};

const packingListReducer = (state = initialState, action) => {
	switch (action.type) {
		case packingListConstants.POST_PACKING_LIST_FAILURE:
			return { ...state, errorMessage: action.payload }
			break;
		case packingListConstants.POST_PACKING_LIST_SUCCESS:
			return {
				...state,
				errorMessage: null,
				orderInfo: action.payload.orderInfoId,
				packingList: action.payload.packingList,
				packingListItem: action.payload.packingListItem,
				sessionId: action.payload.sessionId
			}
			break;
		case packingListConstants.POST_ORDERPACKING_ITEM_NEW_SUCCESS:
			return { ...state, errorMessage: null, responseOrderPackNew: action.payload }
			break;
		case packingListConstants.POST_ORDERPACKING_ITEM_NEW_FAILURE:
			return { ...state, errorMessage: action.payload }
			break;

		case packingListConstants.POST_VIEWALL_ORDERINFOID_SUCCESS:
			return { ...state, errorMessage: null, orderInfo: action.payload }
			break;
		case packingListConstants.POST_VIEWALL_ORDERINFOID_FAILURE:
			return { ...state, errorMessage: action.payload }
			break;

		case packingListConstants.POST_VIEWALL_PACKINGLIST_SUCCESS:
			return { ...state, errorMessage: null, packingList: action.payload }
			break;
		case packingListConstants.POST_VIEWALL_PACKINGLIST_FAILURE:
			return { ...state, errorMessage: action.payload }
			break;

		case packingListConstants.POST_VIEWALL_ITEMSCAN_SUCCESS:
			return { ...state, errorMessage: null, packingListItem: action.payload }
			break;
		case packingListConstants.POST_VIEWALL_ITEMSCAN_FAILURE:
			return { ...state, errorMessage: action.payload }
			break;

		case packingListConstants.POST_ORDERPACKING_FINISH_SUCCESS:
			return { ...state, errorMessage: null, finishResponse: action.payload }
			break;
		case packingListConstants.POST_ORDERPACKING_FINISH_FAILURE:
			return { ...state, errorMessage: action.payload }
			break;

		case packingListConstants.GET_SINGLEDATA_FORPRINT_SUCCESS:
			return { ...state, errorMessage: null, dataPrint: action.payload }
			break;
		case packingListConstants.GET_SINGLEDATA_FORPRINT_FAILURE:
			return { ...state, errorMessage: action.payload }
			break;

		case packingListConstants.POST_MULTIPLEDATA_FORPRINT_SUCCESS:
			return { ...state, errorMessage: null, dataPrint: action.payload }
			break;
		case packingListConstants.POST_MULTIPLEDATA_FORPRINT_FAILURE:
			return { ...state, errorMessage: action.payload }
			break;

		case packingListConstants.POST_ORDERPACKING_CHECK_SUCCESS:
			const dtPayload = action.payload;
			if (dtPayload.response !== undefined && dtPayload.response.failed !== undefined) {
				return {
					...state,
					errorMessage: null,
					orderInfo: action.payload,
					packingList: action.payload,
					packingListItem: action.payload
				}
			}
			return { ...state, errorMessage: null, dataPrint: action.payload }
			break;
		case packingListConstants.POST_ORDERPACKING_CHECK_FAILURE:
			return { ...state, errorMessage: action.payload }
			break;

		case packingListConstants.POST_ORDERPACKING_DELETE_SUCCESS:
			return { ...state, errorMessage: null, deleteResponse: action.payload }
			break;
		case packingListConstants.POST_ORDERPACKING_DELETE_FAILURE:
			return { ...state, errorMessage: action.payload }
			break;

		default:
			return state;
			break;
	}
}

export default packingListReducer;