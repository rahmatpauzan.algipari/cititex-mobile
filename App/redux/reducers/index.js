import { combineReducers } from 'redux';
import login from './loginReducer';
import packingList from './packingListReducer';
import transferPackage from './transferPackageReducer';

const appReducer = combineReducers({
	login,
	packingList,
	transferPackage
});

export default rootReducers = (state, action) => {
	if (action.type === 'USER_LOGOUT') {
		state = undefined;
	}

	return appReducer(state, action);
}