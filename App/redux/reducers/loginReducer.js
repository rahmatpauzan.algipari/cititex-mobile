import { loginConstants } from '../constants';

const initialState = {
	errorMessage: null,
	tokenUser: '',
	data: [],
	logout: null,
};

const loginReducer = (state = initialState, action) => {
	switch (action.type) {
		case loginConstants.LOGIN_REQUEST:
			return { ...state }
			break;
		case loginConstants.LOGIN_FAILURE:
			return { ...state, logout: false, errorMessage: action.error.error }
			break;
		case loginConstants.LOGIN_SUCCESS:
			return { ...state, logout: false, errorMessage: null, tokenUser: action.payload != null && action.payload.data ? action.payload.data.token : null, data: action.payload != null && action.payload.data }
			break;
		case loginConstants.USER_LOGOUT:
			return { ...state, logout: action.payload.logout }
			break;
		case loginConstants.FAIL_LOGOUT:
			return { ...state, logout: action.payload }
			break;

		default:
			return state;
			break;
	}
}

export default loginReducer;
