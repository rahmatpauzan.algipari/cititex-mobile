import { transferPackageConstants } from '../constants';

const initialState = {
	errorMessage: '',
	viewPerPage: []
};

const transferPackageReducer = (state = initialState, action) => {
	switch (action.type) {
		case transferPackageConstants.GET_DATA_PERPAGE_TRANSFERPACKAGE_SUCCESS:
			return { ...state, errorMessage: null, viewPerPage: action.payload }
		case transferPackageConstants.GET_DATA_PERPAGE_TRANSFERPACKAGE_FAILURE:
			return { ...state, errorMessage: action.payload }

		default:
			return state;
	}
}

export default transferPackageReducer;