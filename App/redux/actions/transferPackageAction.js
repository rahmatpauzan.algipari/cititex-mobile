import React from 'react';
import axios from 'axios';
import { transferPackageConstants } from '../constants';
import dtaxios from '../../config/axiosConfig';
import AsyncStorage from '@react-native-community/async-storage';

let source = "";

const fetchDataSuccess = (constant, json, params) => ({
	type: constant,
	payload: json,
	params: params ? params : null,
});

const fetchDataFailure = (constant, error) => ({
	type: constant,
	payload: error
});

export const getViewDataTransferPackage = (param) => {
	source = axios.CancelToken.source();
	return async dispatch => {
		try {
			let response = await dtaxios.get('transferpackage/view/locationid/'+param.locationId+'/page/'+param.page, {
				cancelToken: source.token
			});
			let json = response.data;
			let constant_success = transferPackageConstants.GET_DATA_PERPAGE_TRANSFERPACKAGE_SUCCESS;
			dispatch(fetchDataSuccess(constant_success, json, {paramValue: param}));
			return json;
		} catch (error) {
			let constant_failure = transferPackageConstants.GET_DATA_PERPAGE_TRANSFERPACKAGE_FAILURE;
			dispatch(fetchDataFailure(constant_failure, error));
		}
	}
}

export const getListViewVehicle = () => {
	source = axios.CancelToken.source();
	return async dispatch => {
		try {
			let response = await dtaxios.get('vehicle/view', {
				cancelToken: source.token
			});
			let json = response.data;
			await AsyncStorage.setItem('@listVehicle', JSON.stringify(json));
			let constant_success = transferPackageConstants.GET_VEHICLE_VIEW_SUCCESS;
			dispatch(fetchDataSuccess(constant_success, json, {paramValue: null}));
			return json;
		} catch(e) {
			let constant_failure = packingListConstants.GET_VEHICLE_VIEW_FAILURE;
			dispatch(fetchDataFailure(constant_failure, error));
		}
	}
}

export const getListViewStaff = () => {
	source = axios.CancelToken.source();
	return async dispatch => {
		try {
			let response = await dtaxios.get('staff/view', {
				cancelToken: source.token
			});
			let json = response.data;
			await AsyncStorage.setItem('@listStaff', JSON.stringify(json));
			let constant_success = transferPackageConstants.GET_STAFF_VIEW_SUCCESS;
			dispatch(fetchDataSuccess(constant_success, json, {paramValue: null}));
			return json;
		} catch(e) {
			let constant_failure = packingListConstants.GET_STAFF_VIEW_FAILURE;
			dispatch(fetchDataFailure(constant_failure, error));
		}
	}
}

export const cancelActionTransPack = () => {
	source.cancel('Operation canceled by the user.');
}
