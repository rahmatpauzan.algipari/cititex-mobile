import React from 'react';
import axios from 'axios';
import { loginConstants } from '../constants';
import dtaxios from '../../config/axiosConfig';
import AsyncStorage from '@react-native-community/async-storage';

let source = "";

export const fetchLoginRequest = () => ({ type: loginConstants.LOGIN_REQUEST });

const fetchLoginSuccess = (constant, json, params) => ({
	type: constant,
	payload: json,
	params: params ? params : null,
});

const fetchLoginFailure = (constant, error, params, statusCode) => ({
	type: constant,
	params: params ? params : null,
	error: error,
	status: statusCode
});

export const fetchLogin = (payload) => {
	source = axios.CancelToken.source();
	
	return async dispatch => {
		dispatch(fetchLoginRequest());
		try {
			let response = await dtaxios.post('token', payload, {
				cancelToken: source.token
			});
			let json = response;
			let constant_success = loginConstants.LOGIN_SUCCESS;
			dispatch(fetchLoginSuccess(constant_success, json, payload));
			return json;
		} catch (error) {
			const constant_failure = loginConstants.LOGIN_FAILURE;
			dispatch(fetchLoginFailure(constant_failure, error.response.data, payload, error.response.status));
			return error.response;
		}
	}
}

export const fetchLogout = () => {
	return async dispatch => {
		try {
			AsyncStorage.removeItem('@userToken').then(async (response) => {
				await AsyncStorage.setItem('@isLogin', JSON.stringify(false));
				const first = ['@listOrder', JSON.stringify([])];
				const second = ['@listBarang', JSON.stringify([])];
				const third = ['@listHistory', JSON.stringify([])];
				await AsyncStorage.multiSet([first, second, third]);
				await AsyncStorage.remove('@user');
		      	if (response == null) {
		        	const constant_logout = loginConstants.USER_LOGOUT;
		          	const json = {
		          		logout: true,
		          	}
            		dispatch(fetchLoginSuccess(constant_logout, json, false));
            		return json.logout;
		      	}
		    }).catch(error => console.log(error));
		} catch (error) {
			const constant_failure = loginConstants.FAIL_LOGOUT;
			dispatch(fetchLoginFailure(constant_failure, error));
		}
	}
}

export const cancelFetchLogin = () => {
	source.cancel('Operation canceled by the user.');
}
