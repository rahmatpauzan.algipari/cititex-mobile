import React from 'react';
import axios from 'axios';
import { packingListConstants } from '../constants';
import dtaxios from '../../config/axiosConfig';

let source = "";

const fetchDataSuccess = (constant, json, params) => ({
	type: constant,
	payload: json,
	params: params ? params : null,
});

const fetchDataFailure = (constant, error, params, statusCode) => ({
	type: constant,
	error: error,
	params: params ? params : null,
	status: statusCode
});

export const getDataOrderPackingInfo = (payload) => {
	source = axios.CancelToken.source();

	return async dispatch => {
		try {
			let response = await dtaxios.post('orderpacking/generate', payload, {
				cancelToken: source.token
			});
			let json = response.data;
			let constant_success = packingListConstants.POST_PACKING_LIST_SUCCESS;
			dispatch(fetchDataSuccess(constant_success, json, payload));
			return json;
		} catch (error) {
			let constant_failure = packingListConstants.POST_PACKING_LIST_FAILURE;
			dispatch(fetchDataFailure(constant_failure, error.response.data, payload, error.response.status));
			return error.response;
		}
	}
}

export const scanOrderPackingItemNew = (payload) => {
	source = axios.CancelToken.source();
	
	return async dispatch => {
		try {
			let response = await dtaxios.post('orderpackingitem/new', payload, {
				cancelToken: source.token
			});
			let json = response.data;
			let constant_success = packingListConstants.POST_ORDERPACKING_ITEM_NEW_SUCCESS;
			dispatch(fetchDataSuccess(constant_success, json, payload));
			return json.response;
		} catch (error) {
			let constant_failure = packingListConstants.POST_ORDERPACKING_ITEM_NEW_FAILURE;
			dispatch(fetchDataFailure(constant_failure, error.response.data, payload, error.response.status));
			return error.response;
		}
	}
}

export const orderPackingViewAllOrderInfoId = (payload) => {
	source = axios.CancelToken.source();

	return async dispatch => {
		try {
			let response = await dtaxios.post('orderpacking/view/all/orderinfoid', payload, {
				cancelToken: source.token
			});
			let json = response.data;
			let constant_success = packingListConstants.POST_VIEWALL_ORDERINFOID_SUCCESS;
			dispatch(fetchDataSuccess(constant_success, json, payload));
			return json;
		} catch (error) {
			let constant_failure = packingListConstants.POST_VIEWALL_ORDERINFOID_FAILURE;
			dispatch(fetchDataFailure(constant_failure, error.response.data, payload, error.response.status));
			return error.response;
		}
	}
}

export const orderPackingViewAllPackingList = (payload) => {
	source = axios.CancelToken.source();

	return async dispatch => {
		try {
			let response = await dtaxios.post('orderpacking/view/all/packinglist', payload, {
				cancelToken: source.token
			});
			let json = response.data;
			let constant_success = packingListConstants.POST_VIEWALL_PACKINGLIST_SUCCESS;
			dispatch(fetchDataSuccess(constant_success, json, payload));
			return json;
		} catch (error) {
			let constant_failure = packingListConstants.POST_VIEWALL_PACKINGLIST_FAILURE;
			dispatch(fetchDataFailure(constant_failure, error.response.data, payload, error.response.status));
			return error.response;
		}
	}
}

export const orderPackingViewAllItemScan = (payload) => {
	source = axios.CancelToken.source();

	return async dispatch => {
		try {
			let response = await dtaxios.post('orderpacking/view/all/packingitemscan', payload, {
				cancelToken: source.token
			});
			let json = response.data;
			let constant_success = packingListConstants.POST_VIEWALL_ITEMSCAN_SUCCESS;
			dispatch(fetchDataSuccess(constant_success, json, payload));
			return json;
		} catch (error) {
			let constant_failure = packingListConstants.POST_VIEWALL_ITEMSCAN_FAILURE;
			dispatch(fetchDataFailure(constant_failure, error.response.data, payload, error.response.status));
			return error.response;
		}
	}
}

export const orderPackingFinish = (payload) => {
	return async dispatch => {
		try {
			let response = await dtaxios.post('orderpacking/finish', payload);
			let json = response.data;
			let constant_success = packingListConstants.POST_ORDERPACKING_FINISH_SUCCESS;
			dispatch(fetchDataSuccess(constant_success, json, payload));
			return json;
		} catch (error) {
			let constant_failure = packingListConstants.POST_ORDERPACKING_FINISH_FAILURE;
			dispatch(fetchDataFailure(constant_failure, error.response.data, payload, error.response.status));
			return error.response;
		}
	}
}

// get data for print
/**
 * 1. get single data for print
 */
export const getSingleDataForPrint = (param) => {
	return async dispatch => {
		try {
			let response = await dtaxios.get('orderpacking/view/'+param);
			let json = response.data;
			let constant_success = packingListConstants.GET_SINGLEDATA_FORPRINT_SUCCESS;
			dispatch(fetchDataSuccess(constant_success, json, {paramValue: param}));
			return json;
		} catch (error) {
			let constant_failure = packingListConstants.GET_SINGLEDATA_FORPRINT_FAILURE;
			dispatch(fetchDataFailure(constant_failure, error.response.data, payload, error.response.status));
			return error.response;
		}
	}
}
/**
 * 2. get multiple data for print
 */
export const getMultipleDataForPrint = (payload) => {
	return async dispatch => {
		try {
			let response = await dtaxios.post('orderpacking/inarray/view', payload);
			let json = response.data;
			let constant_success = packingListConstants.POST_MULTIPLEDATA_FORPRINT_SUCCESS;
			dispatch(fetchDataSuccess(constant_success, json, payload));
			return json;
		} catch (error) {
			let constant_failure = packingListConstants.POST_MULTIPLEDATA_FORPRINT_FAILURE;
			dispatch(fetchDataFailure(constant_failure, error.response.data, payload, error.response.status));
			return error.response;
		}
	}
}

export const getResultCheckOrderpacking = (payload) => {
	source = axios.CancelToken.source();

	return async dispatch => {
		try {
			let response = await dtaxios.post('orderpacking/check', payload, {
				cancelToken: source.token
			});
			let json = response.data;
			let constant_success = packingListConstants.POST_ORDERPACKING_CHECK_SUCCESS;
			dispatch(fetchDataSuccess(constant_success, json, payload));
			return json;
		} catch (error) {
			let constant_failure = packingListConstants.POST_ORDERPACKING_CHECK_FAILURE;
			dispatch(fetchDataFailure(constant_failure, error.response.data, payload, error.response.status));
			return error.response;
		}
	}
}

export const deleteDataHistoryPackingList = (payload) => {
	source = axios.CancelToken.source();

	return async dispatch => {
		try {
			let response = await dtaxios.post('orderpackingitem/delete', payload, {
				cancelToken: source.token
			});
			let json = response.data;
			let constant_success = packingListConstants.POST_ORDERPACKING_DELETE_SUCCESS;
			dispatch(fetchDataSuccess(constant_success, json, payload));
			return json;
		} catch (error) {
			let constant_failure = packingListConstants.POST_ORDERPACKING_DELETE_FAILURE;
			dispatch(fetchDataFailure(constant_failure, error.response.data, payload, error.response.status));
			return error.response;
		}
	}
}

export const cancelFetch = () => {
	source.cancel('Operation canceled by the user.');
}
