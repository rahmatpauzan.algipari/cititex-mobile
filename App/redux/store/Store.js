import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from "redux-logger";
import reducers from '../reducers/index';

const reduxLogger = createLogger();
const middleware = [thunk, reduxLogger];
export default createStore(reducers, applyMiddleware(...middleware));