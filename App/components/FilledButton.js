import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text
} from 'react-native';

const FilledButton = ({title, style, onPress, ...props}) => {
  return (
    <TouchableOpacity style={[styles.container, style]} onPress={onPress}>
    	<Text style={styles.text} {...props}>{title.toUpperCase()}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: '#E3562A',
		width: '100%',
		alignItems: 'center',
		justifyContent: 'center',
		padding: 20,
		borderRadius: 10,
	},
	text: {
		color: 'white',
		fontWeight: '500',
		fontSize: 15
	}
});

export default FilledButton;