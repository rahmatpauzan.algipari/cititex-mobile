import React from 'react';
import {
  StyleSheet,
  TextInput
} from 'react-native';

const Input = ({style, ...props}) => {
  return (
    <TextInput {...props} style={[styles.input, style]} />
  );
}

const styles = StyleSheet.create({
	input: {
		// backgroundColor: '#FFFFFF',
		width: '100%',
		padding: 16,
		borderRadius: 12,
		borderWidth: 1,
		borderStyle: 'solid',
		borderColor: '#BEBAB3'
	}
});

export default Input;