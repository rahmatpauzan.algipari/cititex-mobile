import React, { useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  Keyboard
} from 'react-native';
import {
  Title,
  Portal,
  Dialog,
  Button,
  Paragraph,
  ActivityIndicator,
  TextInput,
  HelperText
} from 'react-native-paper';
import { useSelector, useDispatch } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { Formik, Field } from 'formik';
import * as Yup from 'yup';

import { getDataLocation } from '../config/Utils';

const _ = require("lodash");

import SnackbarAlert from './SnackbarAlert';

import {
	scanOrderPackingItemNew,
	orderPackingViewAllOrderInfoId,
	orderPackingViewAllPackingList,
	orderPackingViewAllItemScan
} from '../redux/actions';

const globalData = {
	productId: null,
	orderPackInfo: null,
}

const longBarcodeResult = 26;

const ltrim = (str, char) => {
	const rgxtrim = (!char) ? new RegExp('^\\s+') : new RegExp('^'+char+'+');
	return str.replace(rgxtrim, '');
}

const BarcodeForm = (props) => {
	const dispatch = useDispatch();
	const { orderInfo, packingList, packingListItem, sessionId } = useSelector(state => state.packingList);
	const dtPackingList =  _.orderBy(packingList, ['position'], ['asc']);
	const firstArray = _.first(dtPackingList);

	const [errMessage, setErrMessage] = useState(false);
	const [visibleAlert, setVisibleAlert] = useState(false);
	const [loadingSubmit, setLoadingSubmit] = useState(false);

	const [errProductId, setErrProductId] = useState(false);
	const [errOrderPackInfo, setErrOrderPackInfo] = useState(false);

	const dismissAlert = () => setVisibleAlert(false);

	const dtListOrder = props.dtListOrder;
	const dtListBarang = props.dtListBarang;
	const dtHistory = props.dtHistory;

	const listBarang =  _.orderBy(dtListBarang, ['position'], ['asc']);
	const lstbrg_firstArray = _.first(listBarang);

	const getOrderInfoId = (orderPackInfo) => {
		var stat = null;
		var orderInfoId = null;
		var totalPackInfo = null;
		var totalOrderPackingList = null;
		for (var h = 0; h < dtListOrder.length; h++) {
			const packInfo = JSON.parse(dtListOrder[h].packingInfo);
			for (var i = 0; i < packInfo.length; i++) {
				if (orderPackInfo == packInfo[i].orderPackingInfoId) {
					orderInfoId = dtListOrder[h].orderInfoId;
					totalPackInfo = packInfo[i].total;
					totalOrderPackingList = packInfo[i].totalOrderPackingList;
					stat = 200;
					break;
				} else {
					stat = 404;
				}
			}
			if (stat == 200) {
				break;
			}
		}

		return {
			orderInfoId: orderInfoId,
			totalPackInfo: totalPackInfo,
			totalOrderPackingList: totalOrderPackingList
		};
	}

	const processData = (product_id, order_pack_info, formikBag) => {
		if (dtListOrder.length === 1) {
			const single_orderInfoId = dtListOrder[0].orderInfoId;
			const arrsingle_packingInfo = JSON.parse(dtListOrder[0].packingInfo);
			const resArrSingle_packInfo = arrsingle_packingInfo.filter(d => {
				return d.total > d.totalOrderPackingList;
			});

			if (product_id == '') {
				const message = "Product id tidak boleh kosong...!";
				setErrMessage(message);
				setErrProductId(true);
			} else if (product_id != '' && product_id != lstbrg_firstArray.productId && single_orderInfoId == lstbrg_firstArray.orderInfoId) {
				const message = "Product Id tidak sesuai...";
				setErrMessage(message);
				setErrProductId(true);
			} else {
				setErrProductId(false);
				formikSubmit({
					productId: product_id,
					orderPackInfo: resArrSingle_packInfo[0].orderPackingInfoId,
					quantity: lstbrg_firstArray.quantity,
					orderItemId: lstbrg_firstArray.orderItemId,
					orderInfoId: lstbrg_firstArray.orderInfoId,
					formikBag: formikBag
				});
			}
		} else {
			if (product_id == '') {
				const message = "Product Id tidak boleh kosong...!";
				setErrMessage(message);
				setErrProductId(true);
			} else {
				if (order_pack_info == '') {
					const message = "Order Pack Info tidak boleh kosong...!";
					setErrMessage(message);
					setErrOrderPackInfo(true);
				} else {
					const data = getOrderInfoId(order_pack_info);
					if (data.orderInfoId == lstbrg_firstArray.orderInfoId) {
						setErrProductId(false);
						setErrOrderPackInfo(false);
						formikSubmit({
							productId: product_id,
							orderPackInfo: order_pack_info,
							quantity: lstbrg_firstArray.quantity,
							orderItemId: lstbrg_firstArray.orderItemId,
							orderInfoId: lstbrg_firstArray.orderInfoId,
							formikBag: formikBag
						});
					}
				}
			}
		}
	}

	const formSubmit = (values, formikBag) => {
		const { productId, orderPackInfo } = values;

		if (productId.length == longBarcodeResult) {
			const sliceData = productId.slice(0, 12);
			const dataCode = ltrim(sliceData, '0');
			processData(dataCode, orderPackInfo, formikBag);
		} else {
			processData(productId, orderPackInfo, formikBag);
		}
	}

	const formikSubmit = async (params) => {
		setLoadingSubmit(true);

		const payload = {
			id: params.orderPackInfo,
			orderInfoId: params.orderInfoId,
			quantity: params.quantity,
			productId: params.productId,
			orderItemId: params.orderItemId
		}

		dispatch(scanOrderPackingItemNew(payload)).then(async (res) => {
			if (res.success !== undefined) {
				for(var k = 0; k < dtListOrder.length; k++){
					if (dtListOrder[k].orderInfoId == payload.orderInfoId) {
						const packInfo = JSON.parse(dtListOrder[k].packingInfo);
						for(var m = 0; m < packInfo.length; m++){
							if (packInfo[m].orderPackingInfoId == payload.id) {
								const resultVal = parseInt(packInfo[m].totalOrderPackingList) + parseInt(payload.quantity);

								packInfo[m] = {...packInfo[m], totalOrderPackingList: resultVal}
							}
						}

						const strData = JSON.stringify(packInfo);
						dtListOrder[k] = {...dtListOrder[k], packingInfo: strData}
					}
				}
				props.setDtListOrder(dtListOrder);
				await AsyncStorage.setItem('@listOrder', JSON.stringify(dtListOrder));
				listBarang.splice(0, 1);
				props.setDtListBarang(listBarang);
				await AsyncStorage.setItem('@listBarang', JSON.stringify(listBarang));
				Object.assign(lstbrg_firstArray, {
					orderPackingInfoId: payload.id,
					orderPackingItemId: res.orderPackingItemId
				});
				props.setDtHistory([...dtHistory, lstbrg_firstArray]);
				await AsyncStorage.setItem('@listHistory', JSON.stringify([...dtHistory, lstbrg_firstArray]));

				const resultAllHistory = await AsyncStorage.getItem('@listAllHistory');
				const listAllHistory = JSON.parse(resultAllHistory);
				await AsyncStorage.setItem('@listAllHistory', JSON.stringify([...listAllHistory, lstbrg_firstArray]));

				if (res.success == "FINISH") {
					await AsyncStorage.setItem('@listHistory', JSON.stringify([]));
					props.asyncData();
					const message = "This order packaging info has been finished...!";
					setErrMessage(message);
					setVisibleAlert(true);
				} else {
					if (listBarang.length == 0) {
						await AsyncStorage.setItem('@listHistory', JSON.stringify([]));
						props.asyncData();
						const msg = "List barang telah berhasil diselesaikan..";
						setErrMessage(msg);
						setVisibleAlert(true);
					}
				}
			} else if (res.failed !== undefined) {
				const message = res.failed;
				setErrMessage(message);
				setLoadingSubmit(false);
				setVisibleAlert(true);
			} else {
				const message = res.success;
				setErrMessage(message);
				setVisibleAlert(true);
			}

			params.formikBag.resetForm();

			if (orderInfo !== undefined && orderInfo.length > 0) {
				setLoadingSubmit(false);
				props.onDismiss();
			}
		});
	}

    return (
     	<Portal>
     		<Dialog visible={props.visible} onDismiss={props.onDismiss} style={styles.container}>
     			<Dialog.Title style={styles.dialogTitle}>{ props.title }</Dialog.Title>
     			<Text style={styles.smallText}>Form ini hanya dipakai jika scanner barcode bermasalah!</Text>
     			<Dialog.Content>
     				<Formik
     					initialValues={{
							productId: '',
							orderPackInfo: ''
						}}
						onSubmit={(values,formikBag) => {
							formikBag.setSubmitting(true);
							formSubmit(values, formikBag);
							formikBag.setSubmitting(false);
						}}
						enableReinitialize={true}>
     					{
     						(props) => (
     							<SafeAreaView>
     								<TextInput
	     								mode="outlined"
	     								label="Product ID"
	     								value={props.values.productId}
	     								dense={true}
	     								onChangeText={props.handleChange('productId')}
	     								onBlur={props.handleBlur('productId')}
	     								style={styles.textInput}
	     								required
	     							/>
	     							{ errProductId && <HelperText type="error" padding="none" visible={errProductId}>{ errMessage }</HelperText> }
	     							{
		     							dtListOrder && dtListOrder.length != 1 && <TextInput
		     								mode="outlined"
		     								label="PK-ID"
		     								value={props.values.orderPackInfo}
		     								dense={true}
		     								onChangeText={props.handleChange('orderPackInfo')}
		     								onBlur={props.handleBlur('orderPackInfo')}
		     								style={styles.textInput}
		     							/>
	     							}
	     							{ errOrderPackInfo && <HelperText type="error" padding="none" visible={errOrderPackInfo}>{ errMessage }</HelperText> }
	     							<Button mode="contained" uppercase={true} style={styles.buttonSubmit} loading={loadingSubmit} disabled={props.isSubmitting ? true : false} labelStyle={styles.labelInput} onPress={props.handleSubmit}>
	     								Submit
	     							</Button>
     							</SafeAreaView>
     						)
     					}
     				</Formik>
     			</Dialog.Content>
     		</Dialog>
     		<SnackbarAlert visible={visibleAlert} onDismiss={dismissAlert} duration={3000} backgroundColor="#000" textColor="#FFFFFF" message={errMessage} setActionOnPress={dismissAlert} />
     	</Portal>
    );
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: '#fff'
	},
	dialogTitle: {
		color: '#000',
		marginBottom: 0
	},
	smallText: {
		color: '#000',
		fontStyle: 'italic',
		fontSize: 12,
		marginLeft: 25
	},
	buttonSubmit: {
		marginTop: 20
	},
	labelInput: {
		color: '#fff'
	},
	textInput: {
		backgroundColor: '#fff',
		height: 50
	}
});

export default BarcodeForm;