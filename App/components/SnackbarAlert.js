import React from 'react';
import { Snackbar } from 'react-native-paper';

const SnackbarAlert = (props) => {
  	return (
  		<Snackbar
  			visible={props.visible}
			onDismiss={props.onDismiss}
			duration={props.duration}
			theme={{
				colors: {
					surface: props.textColor,
					accent: props.textColor
				}
			}}
			style={{
				backgroundColor: props.backgroundColor
			}}
			action={{
				label: 'Close',
				onPress: props.setActionOnPress
			}}
		>
  			{ props.message }
  		</Snackbar>
  	)
}

export default SnackbarAlert;