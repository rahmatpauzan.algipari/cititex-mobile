import React, { useRef, useContext, useCallback, useEffect } from 'react';
import { Animated, StyleSheet, Text, View, I18nManager } from 'react-native';
import MdIcon from 'react-native-vector-icons/MaterialCommunityIcons';

import { RectButton } from 'react-native-gesture-handler';

import Swipeable from 'react-native-gesture-handler/Swipeable';
import { SwipeContext } from './SwipeProvider';

const AnimatedIcon = Animated.createAnimatedComponent(MdIcon);

export default function SwipeableRow({ children, rightButtons = [], itemKey }) {
  const _swipeableRow = useRef(null);
  const { openedItemKey, setOpenedItemKey, isFocusedScreen } = useContext(
    SwipeContext,
  );

  const close = () => {
    if (_swipeableRow.current) {
      _swipeableRow.current.close();
    }
  };

  const handleSwipe = () => {
    setOpenedItemKey(itemKey);
  };

  useEffect(() => {
    // If another item is opened, close this one
    if (openedItemKey && itemKey !== openedItemKey) {
      close();
    }
  }, [itemKey, openedItemKey]);

  useEffect(() => {
    close();
  }, [isFocusedScreen]);

  const renderButtons = useCallback((buttons, progress) => {
    return (
      <View
        style={{
          width: 64 * buttons.length,
          flexDirection: I18nManager.isRTL ? 'row-reverse' : 'row',
        }}>
        {buttons.map(({ text, color, iconName, x, onPress }) => {
          const trans = progress.interpolate({
            inputRange: [0, 1],
            outputRange: [x, 0],
          });
          const pressHandler = () => {
            close();
            if (onPress) {
              onPress(text);
            }
          };
          return (
            <Animated.View
              key={text}
              style={{
                flex: 1,
                transform: [{ translateX: trans }],
              }}>
              <RectButton
                style={[styles.rightAction, { backgroundColor: color }]}
                onPress={pressHandler}>
                <AnimatedIcon name={iconName} size={24} color="#dd2c00" />
              </RectButton>
            </Animated.View>
          );
        })}
      </View>
    );
  }, []);

  const renderRightButtons = useCallback(
    (progress) => {
      return renderButtons(rightButtons, progress);
    },
    [rightButtons],
  );

  return (
    <Swipeable
      ref={_swipeableRow}
      friction={2}
      leftThreshold={30}
      rightThreshold={40}
      onSwipeableWillOpen={handleSwipe}
      renderRightActions={renderRightButtons}>
      {children}
    </Swipeable>
  );
}

const styles = StyleSheet.create({
  leftAction: {
    flex: 1,
    backgroundColor: '#497AFC',
    justifyContent: 'center',
  },
  rightAction: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
});