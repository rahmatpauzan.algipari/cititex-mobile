import React, { createContext, useMemo, useState } from 'react';

const swipeContextValues = {
  openedItemKey: '',
  setOpenedItemKey: () => {},
  isFocusedScreen: false,
};

export const SwipeContext = createContext(swipeContextValues);

export default function SwipeProvider({
  initialOpenedItemKey = '',
  children,
  isFocusedScreen,
}) {
  const [openedItemKey, setOpenedItemKey] = useState(initialOpenedItemKey);

  const value = useMemo(() => {
    return {
      openedItemKey,
      setOpenedItemKey,
      isFocusedScreen,
    };
  }, [openedItemKey, isFocusedScreen]);

  return (
    <SwipeContext.Provider value={value}>{children}</SwipeContext.Provider>
  );
}

SwipeProvider.Context = SwipeContext;