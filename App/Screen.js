import React from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	button: {
		paddingHorizontal: 20,
		paddingVertical: 10,
		marginVertical: 10,
		borderRadius: 5
	}
});

const ScreenContainer = ({ children }) => {
	return (
		<View style={styles.container}>{children}</View>
	);
}

export const Details = () => {
	return (
		<ScreenContainer>
			<Text>Details Screen</Text>
		</ScreenContainer>
	);
}

export const Profile = ({ navigation }) => {
	return (
		<ScreenContainer>
			<Text>Profile Screen</Text>
			<Button title="Navigate to details at home" onPress={() => {
				navigation.navigate("TabHome", {
					screen: "Details",
					params: { name: "Navigate from Profile" }
				});
			}} />
		</ScreenContainer>
	);
}