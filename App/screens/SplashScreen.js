import React from 'react';
import {
	View,
	Text,
	StyleSheet,
	Image
} from 'react-native';
import { getVersion } from 'react-native-device-info';
import Logo from '../../assets/images/cititex-new-logo-smallest.jpg';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#ffffff"
	},
	SplashText: {
		fontFamily: "Rubik",
		fontStyle: "normal",
		fontWeight: "bold",
		fontSize: 40,
		lineHeight: 115,
		textAlign: "center",
		letterSpacing: -1,
		color: "#1F1F1F",
	}
});

const SplashScreen = () => {
	return (
		<View style={styles.container}>
			<Image source={Logo} />
			<Text style={styles.SplashText}>Cititex Inventory</Text>
			<View style={{
				alignItems: 'center',
				marginTop: 60
			}}>
				<Text>App Version</Text>
				<Text>{ getVersion() }</Text>
			</View>
		</View>
	);
}

export default SplashScreen;
