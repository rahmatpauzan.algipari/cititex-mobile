import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Alert } from 'react-native';
import { Button, Snackbar } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import { getVersion } from 'react-native-device-info';

import Heading from '../components/Heading';
import Input from '../components/Input';

import { useSelector, useDispatch } from 'react-redux';
import { Formik } from 'formik';
import * as yup from 'yup';

import {
	fetchLogin,
	cancelFetchLogin
} from '../redux/actions';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 20,
		paddingTop: 120,
		alignItems: 'center',
		backgroundColor: 'white',
	},
	title: {
		marginBottom: 10,
		color: '#2e2e2e'
	},
	input: {
		marginVertical: 8,
		color: '#78746D'
	},
	bodyText: {
		marginBottom: 30,
		fontFamily: 'Rubik',
		fontStyle: 'normal',
		fontWeight: 'normal',
		fontSize: 14,
		lineHeight: 21,
		textAlign: 'center',
		color: '#78746D'
	},
	loginButton: {
		marginVertical: 10,
		width: wp('90%'),
	},
	errorText: {
		fontSize: 12,
		color: 'red',
	},
});

const loginValidationSchema = yup.object().shape({
	username: yup
		.string()
		.required('Username is required'),
	password: yup
		.string()
		.min(3, ({ min }) => `Password must be at least ${min} charachters`)
		.required('Password is required'),
});

const LoginScreen = (props) => {
	const dispatch = useDispatch();
	const navigation = props.navigation;
	const [loading, setLoading] = useState(false);
	const { errorMessage } = useSelector(state => state.login);
	const [hidePass, setHidePass] = useState(true);
	const [visibleAlert, setVisibleAlert] = useState(false);

	const submitHandler = async (values, formikBag) => {
		setLoading(true);
		const payload = {
			username: values.username,
			password: values.password,
			version: getVersion()
		}

		await dispatch(fetchLogin(payload))
		.then(async (result) => {
			if (result.status === 200) {
				const jsonData = result.data;
				await AsyncStorage.setItem('@isLogin', JSON.stringify(true));
				await AsyncStorage.setItem('@userToken', jsonData.token);
				await AsyncStorage.setItem('@user', payload.username);
				await AsyncStorage.setItem('@roles', JSON.stringify(jsonData.roles));
				await AsyncStorage.setItem('@location', JSON.stringify(jsonData.location));
				await AsyncStorage.setItem('@locationId', JSON.stringify(jsonData.locationId));
				await AsyncStorage.setItem('@workLocationId', JSON.stringify(jsonData.workLocationId));
				formikBag.resetForm();
				navigation.navigate('Home');
				setLoading(false);

				if (errorMessage) {
					setVisibleAlert(true);
				}
			} else if (result.status === 401) {
				setLoading(false);
				setVisibleAlert(true);
			}
		})
		.catch(error => {
			setLoading(false);
			Alert.alert("Sorry, something went wrong.", error.message);
		});
	
	}

	const hideDialogAlert = () => setVisibleAlert(false);

	return (
		<View style={styles.container}>
			<Heading style={styles.title}>Cititex App</Heading>
			<Text style={styles.bodyText}>Login with your Cititex Account</Text>
			<Formik
				validationSchema={loginValidationSchema}
		    	initialValues={{
		    		username: '',
		    		password: ''
		    	}}
		    	onSubmit={(values, formikBag) => {
		        	submitHandler(values, formikBag)
		    	}}
			>
				{
					({ handleChange, handleBlur, handleSubmit, values, errors, touched, isValid }) => (
						<>
							<Input
					    		style={styles.input}
					    		name="username"
					    		onChangeText={handleChange('username')}
					    		onBlur={handleBlur('username')}
					    		value={values.username}
					    		placeholder="Username"
					    		placeholderTextColor="#78746D"
					    	/>
					    	{(errors.username && touched.username) && <Text style={styles.errorText}>{errors.username}</Text>}

						    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
						    	<Input
						    		style={styles.input}
						    		name="password"
						    		onChangeText={handleChange('password')}
						    		onBlur={handleBlur('password')}
						    		value={values.password}
						    		placeholder="Password"
						    		placeholderTextColor="#78746D"
						    		secureTextEntry={hidePass ? true : false}
						    	/>
						    	<Icon name={hidePass ? 'eye-off' : 'eye'} style={{ position: 'absolute', right: 15 }} onPress={() => setHidePass(!hidePass)} size={25} />
						    </View>
					    	{(errors.password && touched.password) && <Text style={styles.errorText}>{errors.password}</Text>}
					    	<Button mode="contained" uppercase={true} contentStyle={styles.loginButton} labelStyle={{ fontSize: 15, color: '#FFFFFF' }} style={{ backgroundColor: '#E3562A', borderRadius: 12 }} loading={loading} onPress={() => {
					    		if (loading) {
					    			cancelFetchLogin();
					    			setLoading(false);
					    		} else {
					    			handleSubmit();
					    		}
					    	}}>
					    		Sign In
					    	</Button>
						</>
					)
				}
			</Formik>
			<Snackbar
				visible={visibleAlert}
				onDismiss={hideDialogAlert}
				duration={3000}
				theme={{
					colors: {
						accent: '#FFFFFF'
					}
				}}
				style={{
					backgroundColor: '#E3562A'
				}}
				action={{
					label: 'Close',
					onPress: () => {
						setVisibleAlert(false);
					}
				}}
			>
				{ errorMessage }
			</Snackbar>
		</View>
	);
}

export default LoginScreen;
