import React, { useState, useEffect } from 'react';
import {
	View,
	SafeAreaView,
	ScrollView,
	Alert,
	StyleSheet
} from 'react-native';
import {
	Title,
	Text,
	Appbar,
	Button,
	TextInput,
	DataTable,
	IconButton
} from 'react-native-paper';
import NetInfo from "@react-native-community/netinfo";
import { useSelector, useDispatch } from 'react-redux';
import DropDownPicker from 'react-native-dropdown-picker';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Orientation from 'react-native-orientation-locker';
import AsyncStorage from '@react-native-community/async-storage';
import { useFormik } from 'formik';
import * as yup from 'yup';

import Input from '../../components/Input';

import {
	getListViewStaff,
	getListViewVehicle
} from '../../redux/actions';

const CreateTransPackScreen = ({ navigation }) => {
	const dispatch = useDispatch();

	const [listStaff, setListStaff] = useState([]);
	const [listVehicle, setListVehicle] = useState([]);
	const [listLocation, setListLocation] = useState([]);

	const [openVehicle, setOpenVehicle] = useState(false);
	const [valueVehicle, setValueVehicle] = useState(null);

	const [openStaff, setOpenStaff] = useState(false);
	const [valueStaff, setValueStaff] = useState([]);

	const [openLocation, setOpenLocation] = useState(false);
	const [valueLocation, setValueLocation] = useState(null);

	const [inputList, setInputList] = useState([{ packId: "" }]);

	useEffect(() => {
		Orientation.lockToPortrait();
		getListVehicle();
	}, []);

	const getListVehicle = async () => {
		const resultLocation = await AsyncStorage.getItem('@location');
		const dataLocation = JSON.parse(resultLocation);
		const arrLocate = [];
		if (Array.isArray(dataLocation)) {
			dataLocation.map(item => {
				arrLocate.push({
					label: item.name,
					value: item.id
				});
			})
		}
		setListLocation(arrLocate);

		NetInfo.fetch().then(async (state) => {
			if (state.isConnected) {
				const resultStaff = await dispatch(getListViewStaff());
				const arrStaff = [];
				if (Array.isArray(resultStaff)) {
					resultStaff.map(item => {
						arrStaff.push({
							label: item.username,
							value: item.id
						});
					})
				}
				setListStaff(arrStaff);

				const resultVehicle = await dispatch(getListViewVehicle());
				const arrVechile = [];
				if (Array.isArray(resultVehicle)) {
					resultVehicle.map(item => {
						arrVechile.push({
							label: item.name,
							value: item.id
						});
					})
				}
				setListVehicle(arrVechile);
			} else {
				const resultStaff = await AsyncStorage.getItem('@listStaff');
				const dataStaff = JSON.parse(resultStaff);
				const arrStaff = [];
				if (Array.isArray(dataStaff)) {
					dataStaff.map(item => {
						arrStaff.push({
							label: item.username,
							value: item.id
						});
					})
				}
				setListStaff(arrStaff);

				const resultVehicle = await AsyncStorage.getItem('@listVehicle');
				const dataVehicle = JSON.parse(resultVehicle);
				const arrVechile = [];
				if (Array.isArray(dataVehicle)) {
					dataVehicle.map(item => {
						arrVechile.push({
							label: item.name,
							value: item.id
						});
					})
				}
				setListVehicle(arrVechile);
			}
		});
	}

	const formik = useFormik({
		initialValues: {
			vehicleOpt: '',
			staffOpt: '',
			locationOpt: '',
			description: '',
			packingId: []
		}
	});

	return (
		<View style={styles.container}>
			<Appbar.Header style={styles.appBarheader}>
				<Appbar.BackAction onPress={() => navigation.goBack()} />
				<Appbar.Content title="Add Transfer Package" />
			</Appbar.Header>
			<SafeAreaView style={{
				flex: 1,
				padding: 8,
				backgroundColor: '#fff'
			}}>
				<Title>Vehicle</Title>
				<DropDownPicker
					placeholder="Select"
					open={openVehicle}
					value={valueVehicle}
					items={listVehicle}
					setOpen={setOpenVehicle}
					setValue={setValueVehicle}
					zIndex={5000}
					zIndexInverse={3000}
				/>

				<Title>Staff</Title>
				<DropDownPicker
					placeholder="Select"
					mode="BADGE"
					multiple={true}
					min={0}
					max={5}
					open={openStaff}
					value={valueStaff}
					items={listStaff}
					setOpen={setOpenStaff}
					setValue={setValueStaff}
					zIndex={4000}
					zIndexInverse={4000}
				/>

				<Title>Location</Title>
				<DropDownPicker
					placeholder="Select"
					open={openLocation}
					value={valueLocation}
					items={listLocation}
					setOpen={setOpenLocation}
					setValue={setValueLocation}
					zIndex={3000}
					zIndexInverse={5000}
				/>

				<Title>Description</Title>
				<View style={{
					borderColor: '#000',
					borderWidth: 1,
					borderRadius: 5,
					padding: 5
				}}>
					<TextInput
						mode="flat"
						placeholder="Type description in here...."
						multiline={true}
						numberOfLines={5}
						value={formik.values.description}
						onChangeText={formik.handleChange('description')}
						blur={formik.handleBlur('description')}
						style={{
							backgroundColor: '#fff',
							color: '#000'
						}}
					/>
				</View>

				<DataTable style={{
					paddingTop: 10
				}}>
					<DataTable.Header>
						<DataTable.Title style={{ justifyContent: 'center', alignItems: 'center' }}>#</DataTable.Title>
    					<DataTable.Title style={{ justifyContent: 'center', alignItems: 'center' }}>Packing ID</DataTable.Title>
    					<DataTable.Title style={{ justifyContent: 'center', alignItems: 'center' }}>Action</DataTable.Title>
					</DataTable.Header>
					{
						inputList.map((x, y) => {
							return (
								<DataTable.Row key={y}>
									<DataTable.Cell
										numeric
										style={{
											fontSize: 20,
											justifyContent: 'center',
											alignItems: 'center'
										}}>
										<Text>{y+1}</Text>
									</DataTable.Cell>
									<DataTable.Cell
										style={{
											fontSize: 20,
											justifyContent: 'center',
											alignItems: 'center'
										}}>
										<TextInput
											mode="flat"
											dense={true}
											style={{
												backgroundColor: '#fff',
												width: '100%',
												minHeight: 48,
												paddingVertical: 3
											}}
											placeholder="Type here..."
											value={x.packId}
										/>	
									</DataTable.Cell>
									<DataTable.Cell
										style={{
											fontSize: 20,
											justifyContent: 'center',
											alignItems: 'center'
										}}>
										{ inputList.length - 1 === y && <IconButton icon="plus-box-outline" color="#1D76F0" size={30} onPress={() => Alert.alert('test pressed plus')} /> }
										{ inputList.length !== 1 && <IconButton icon="close-box-outline" color="#E53232" size={30} onPress={() => Alert.alert('test pressed close')} /> }
									</DataTable.Cell>
								</DataTable.Row>
							);
						})
					}
				</DataTable>
			</SafeAreaView>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	appBarheader: {
		backgroundColor: '#191919',
	},
	content: {
		borderColor: '#000',
		borderWidth: 1,
		borderRadius: 8
	}
});

export default CreateTransPackScreen;