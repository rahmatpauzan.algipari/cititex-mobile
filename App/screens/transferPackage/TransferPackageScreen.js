import React, { useState, useEffect } from 'react';
import {
	View,
	SafeAreaView,
	ScrollView,
	StyleSheet,
	Alert,
	FlatList
} from 'react-native';
import {
	Text,
	Appbar,
	Button,
	List,
	IconButton,
	ActivityIndicator
} from 'react-native-paper';
import { useSelector, useDispatch } from 'react-redux';
import { useFocusEffect } from '@react-navigation/native';
import Orientation from 'react-native-orientation-locker';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from "@react-native-community/netinfo";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import {
	getViewDataTransferPackage
} from '../../redux/actions';

const EmptyListData = () => (
	<View style={{
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	}}>
		<Text style={{ fontWeight: '100', color: 'lightgray', fontSize: 18, fontStyle: 'italic' }}>Data is Empty..</Text>
	</View>
)

const TransferPackageScreen = ({ navigation }) => {
	const dispatch = useDispatch();
	const [dataList, setDataList] = useState([]);
	const [loading, setLoading] = useState(false);

	const [counter, setCounter] = useState(1);

	useFocusEffect(
		React.useCallback(() => {
			findDataTransferPackage(counter);
			Orientation.lockToPortrait();
		}, [counter])
	);

	const addLeadingZero = (n) => {
		if (n <= 9) { return "0" + n; }
		return n;
	}

	const counterForward = () => {
		const cal = counter + 1;
		setCounter(cal);
	}

	const counterPrevious = () => {
		if (counter > 1) {
			const cal = counter - 1;
			setCounter(cal);
		}
	}

	const findDataTransferPackage = async (page) => {
		setLoading(true);
		const dtArr = new Array();
		const locationId = await AsyncStorage.getItem('@locationId');
		NetInfo.fetch().then(async (state) => {
			if (state.isConnected) {
				const data = await dispatch(getViewDataTransferPackage({locationId: locationId, page: page}))
				.catch(error => {
					setLoading(false);
					Alert.alert("Sorry, something went wrong.", error.message);
				});
				if (Array.isArray(data)) {
					setLoading(false);
					setDataList(data);
				}
			} else {
				setLoading(false);
				Alert.alert("Sorry...", "You're offline right now, please check your internet connection");
			}
		});
	}

	return (
		<View style={styles.container}>
			<Appbar.Header style={styles.appBarheader}>
				<Appbar.BackAction onPress={() => navigation.goBack()} />
				<Appbar.Content title="Transfer Package" />
			</Appbar.Header>
			<SafeAreaView style={{
					paddingTop: 10,
					paddingLeft: 5,
					paddingRight: 5,
					flexDirection: 'row'
				}}>
				<Button
					mode="contained"
					compact={true}
					color="#000"
					style={{
						margin: 2
					}}
					contentStyle={{
						flexWrap: 'wrap'
					}}
					onPress={() => navigation.navigate('CreateTransPack')}
				>
					{'Add Transfer Package'}
				</Button>
				<Button
					mode="contained"
					compact={true}
					color="#000"
					style={{
						margin: 2
					}} contentStyle={{
						flexWrap: 'wrap'
					}} onPress={() => Alert.alert('test button')}
				>
					{'Transfer Package Receive'}
				</Button>
			</SafeAreaView>
			<View style={styles.containerContent}>
				<View style={{
					paddingHorizontal: 10,
					flexDirection: 'row',
					marginBottom: 15
				}}>
					<Button icon="skip-previous" mode="contained" style={{
						borderRadius: 30
					}} onPress={counterPrevious}>
						Prev
					</Button>
					<View style={{
						justifyContent: 'center',
						alignItems: 'center',
						borderColor: '#000',
						borderWidth: 1,
						borderRadius: 5,
						paddingHorizontal: 15,
						marginHorizontal: 10
					}}>
						<Text style={{ fontSize: 18 }}>{ counter }</Text>
					</View>
					<Button icon="skip-forward" mode="contained" style={{
						borderRadius: 30
					}} contentStyle={{ flexDirection: 'row-reverse' }} onPress={counterForward}>
						Next
					</Button>
				</View>
				{
					loading ? (
						<View style={{
							flex: 1,
							justifyContent: 'center',
							alignItems: 'center'
						}}>
							<ActivityIndicator animating={true} color="#000" size="large" />
							<Text style={{ marginTop: 20 }}>{'Loading...'}</Text>
						</View>
					) : (
						<FlatList
							data={dataList}
							keyExtractor={(item, index) => index}
							contentContainerStyle={{
								padding: 10,
								flexGrow: 1
							}}
							ListEmptyComponent={<EmptyListData/>}
							renderItem={(item, index) => {
								const dataItem = item.item;
								const dt = new Date(dataItem.addTime);
								const monthName = dt.toLocaleString('default', { month: 'long' });
								const date = monthName + ' ' + addLeadingZero(dt.getDate()) + ', ' + dt.getFullYear();

								return (
									<View style={{
											backgroundColor: 'rgba(255,255,255, 0.8)',
											padding: 5,
											marginBottom: 20,
											borderColor: '#000',
											borderWidth: 1,
											borderRadius: 10,
											width: '100%'
										}}>
										<View style={{
												flexDirection: 'row',
												justifyContent: 'space-between'
											}}>
											<Text style={{ fontSize: 20 }}>{ 'ID ' + dataItem.id }</Text>
											<IconButton
												icon="printer"
												onPress={() => Alert.alert('test pressed '+item.index)}
												style={{ margin: 0 }} />
										</View>
										<View style={{
												flexDirection: 'row',
												paddingVertical: 5
											}}>
											<View style={{
													width: '50%'
												}}>
												<Text style={{ fontSize: 15, fontWeight: '700' }}>{ 'Location' }</Text>
												<Text style={{ fontSize: 15 }}>{ dataItem.locationName }</Text>
											</View>
											<View style={{
													width: '50%'
												}}>
												<Text style={{ fontSize: 15, fontWeight: '700' }}>{ 'Vechile' }</Text>
												<Text style={{ fontSize: 15 }}>{ dataItem.vehicle }</Text>
											</View>
										</View>
										<View style={{
											flexDirection: 'row',
											paddingVertical: 5
										}}>
											<View style={{
													width: '50%'
												}}>
												<Text style={{ fontSize: 15, fontWeight: '700' }}>{ 'Destination' }</Text>
												<Text style={{ fontSize: 15 }}>{ dataItem.destinationLocationName }</Text>
											</View>
											<View style={{
													width: '50%'
												}}>
												<Text style={{ fontSize: 15, fontWeight: '700' }}>{ 'Status' }</Text>
												<Text style={{ fontSize: 15 }}>{ dataItem.status }</Text>
											</View>
										</View>
										<View>
											<Text style={{ fontSize: 15 }}>
												<Text style={{ fontSize: 15, fontWeight: '700' }}>{ 'Description : ' }</Text>
												{ dataItem.destinationDescription }
											</Text>
										</View>
										<View style={{
											flexDirection: 'row',
											justifyContent: 'space-between',
											paddingVertical: 5
										}}>
											<View style={{
												width: '50%'
											}}>
												<Text style={{ fontSize: 15, fontWeight: '700' }}>{ 'Total' }</Text>
												<Text style={{ fontSize: 15 }}>{ dataItem.total }</Text>
											</View>
											<View style={{
													width: '50%'
												}}>
												<Text style={{ fontSize: 15, fontWeight: '700' }}>{ 'Add Time' }</Text>
												<Text style={{ fontSize: 15 }}>{ date }</Text>
											</View>
										</View>
									</View>
								)
							}}
						/>
					)
				}
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	appBarheader: {
		backgroundColor: '#191919',
	},
	containerContent: {
		flex: 1,
		padding: 5,
		paddingTop: 30,
		backgroundColor: '#fff'
	}
});

export default TransferPackageScreen;