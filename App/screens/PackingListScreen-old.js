import React, { useState, useEffect } from 'react';
import {
  	View,
	ScrollView,
	Text,
	StyleSheet,
	TouchableOpacity,
	Alert,
	Platform,
	FlatList,
	TouchableHighlight,
	SafeAreaView
} from 'react-native';
import {
	Appbar,
	Button,
	IconButton,
	Subheading,
	Card,
	Title,
	Paragraph,
	Dialog,
	Portal,
	Snackbar,
	DataTable,
	ActivityIndicator
} from 'react-native-paper';
import Orientation from 'react-native-orientation';

import { RNCamera } from 'react-native-camera';
import BarcodeMask from 'react-native-barcode-mask';
import { useSelector, useDispatch } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import AwesomeAlert from 'react-native-awesome-alerts';
import RNBeep from 'react-native-a-beep';
import { useFocusEffect } from '@react-navigation/native';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { Formik } from 'formik';
import * as Yup from 'yup';
import { getThermalPrinterIp } from '../config/Utils';

import SnackbarAlert from '../components/SnackbarAlert';

import {
	getDataOrderPackingInfo,
	scanOrderPackingItemNew,
	orderPackingViewAllOrderInfoId,
	orderPackingViewAllPackingList,
	orderPackingViewAllItemScan,
	orderPackingFinish,
	getSingleDataForPrint,
	getMultipleDataForPrint,
	getResultCheckOrderpacking
} from '../redux/actions';

const _ = require("lodash");

const isIOS = Platform.OS === 'ios';

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	appHeader: {
		backgroundColor: '#191919',
	},
	sizeBoxCameraScan: {
		flex: 1
	},
	styleBoxCameraScan: {
		justifyContent: 'center',
		alignItems: 'center',
		margin: 3
	},
  	styleboxOrderInfo: {
		borderWidth: 1,
		borderColor: '#000',
		backgroundColor: '#fff',
		borderRadius: 5,
		margin: 3
	},
	styleBoxPackingList: {
		borderWidth: 1,
		borderColor: '#000',
		backgroundColor: '#fff',
		borderRadius: 5,
		margin: 3
	},
  	buttonTextStyle: {
        color: 'white',
        fontWeight: 'bold',
    },
    buttonFlash: {
    	borderWidth: 1,
    	borderColor: '#000'
    },
    buttonClose: {
    	borderWidth: 1,
    	borderColor: '#000',
    	width: '30%'
    },
    buttonOpenCameraScan: {
		borderWidth: 1,
		borderColor: '#000',
		fontSize: 21,
		fontWeight: 'bold'
	},
	buttonOnCamera: {
		position: 'absolute',
		top: hp('4%'),
		width: wp('100%'),
		flexDirection: 'column',
		justifyContent: 'center'
	},
	buttonFloatOnCameraBox: {
		backgroundColor: 'white',
		borderWidth: 1,
		borderColor: '#fff'
	},
    containerButton: {
    	flex: 0.3,
    	borderWidth: 1,
	    flexDirection: 'row',
	    justifyContent: 'space-evenly',
	    alignItems: 'center'
	},
	containerOrderPacking: {
		// flex: 0.8,
		height: 'auto',
		width: 'auto'
	},
	containerOrderPackStyle: {
		flexDirection: 'row',
		padding: 3
	},
	listOrderPackStyle: {
		height: hp('8.7%'),
		padding: 2,
	},
	contentContainerStyle: {
		alignItems: 'center',
		paddingTop: 0,
		paddingBottom: 12,
		width: wp('20%')
	},
	scrollViewStyle: {
		textAlign: 'center',
		borderWidth: 1,
		borderColor: '#000',
		borderRadius: 8
	},
	textSubHeading: {
		fontSize: 13,
		color: 'black',
	},
	textInScrollView: {
		fontSize: 19,
		textAlign: 'center',
	},
	containerFinishPacking: {
		flex: 3
	},
	textBlack: {
		color: '#000',
		paddingHorizontal: 16
	},
	flexKeyNameColumn: {
		flexBasis: 110,
		flexShrink: 1,
		flexGrow: 0
	},
	flexColumnSeparate: {
		flexBasis: 20,
		flexShrink: 1,
		flexGrow: 0
	}
});

const Line = () => (
	<View style={{ height: 1, backgroundColor: '#000', alignSelf: 'stretch' }} />
)

const PackingListScreenOld = ({ navigation }) => {
	const dispatch = useDispatch();

	const locationId = useSelector(state => state.login.data.locationId);
	const { orderInfo, packingList, responseOrderPackNew, packingListItem, sessionId, dataPrint } = useSelector(state => state.packingList);

	useFocusEffect(
		React.useCallback(() => {
			startingGenerate(locationId);
			Orientation.lockToPortrait();
		}, [locationId])
	);

	const dtPackingList =  _.orderBy(packingList, ['position'], ['asc']);
	const firstArray = _.first(dtPackingList);

	const dtPackingListFinish = _.orderBy(
		packingListItem,
		['orderItemId'],
		['desc']
	);

	const [scan, setScan] = useState(false);
	const [scans, setScans] = useState(true);
	const [camera, setCamera] = useState(null);
	const [codeScan, setCodeScan] = useState(false);
	const [scanResult, setScanResult] = useState(false);
	const [flashButton, setFlashButton] = useState(false);
	const [orderInfoId, setOrderInfoId] = useState(null);
	const [orderPackInfo, setOrderPackInfo] = useState(null);
	const [totalPackInfo, setTotalPackInfo] = useState(null);
	const [prevProductId, setPrevProductId] = useState(null);
	const [totalOrderPackingList, setTotalOrderPackingList] = useState(null);
	const [typeCamera, setTypeCamera] = useState(RNCamera.Constants.Type.back);
	const [torchLight, setTorchLight] = useState(RNCamera.Constants.FlashMode.off);
	const [visible, setVisible] = useState(false);
	const [visibleAlert, setVisibleAlert] = useState(false);

	const [alertError, setAlertError] = useState(false);
	const [dataAlertError, setDataAlertError] = useState(null);

	const [visibleErrPrinter, setVisibleErrPrinter] = useState(false);
	const [errPrinter, setErrPrinter] = useState(null);

	const [loadingPrintAll, setLoadingPrintAll] = useState(false);
	const [loadingInfoDataPrint, setLoadingInfoDataPrint] = useState(false);
	const [disablePrint, setDisablePrint] = useState(false);

	const [loadingGenerate, setLoadingGenerate] = useState(false);

	const startingGenerate = (location_id) => {
		AsyncStorage.getItem('@locationId')
		.then(res => {
			dispatch(getResultCheckOrderpacking({ locationId: location_id === undefined ? res : location_id }))
			.then(resCheck => {
				if (resCheck.response.success !== undefined) {
					dispatch(getDataOrderPackingInfo({ locationId: location_id === undefined ? res : location_id }))
					.then(ress => {
						setVisibleAlert(true);
					});
				} else if (resCheck.response.failed !== undefined) {
					const message = "Anda tidak memiliki order packing untuk dihandle, silahkan generate..!";
					showAlertError(message);
				}
			});
		});
	}

	const printDataOrderPack = async () => {
		setDisablePrint(true);
		console.log('click');
		var ipThermalPrinter = await getThermalPrinterIp();
		var cmd = '^XA'+
				'^CF0,40'+
				'^FO50,100^FD'+dataPrint.shippingName+'^FS'+
				'^FO50,150^GB700,3,3^FS'+
				'^CFA,30'+
				'^FO50,200^FDOR-ID: '+dataPrint.orderInfoId+'^FS'+
				'^FO50,240^FDPK-ID: '+dataPrint.id+'^FS'+
				'^FO50,300^FD'+dataPrint.orderInfoReceiverName+' ('+dataPrint.orderInfoReceiverContact+')^FS'+
				'^FO50,340^FDSender : '+dataPrint.orderInfoSenderName+' ('+dataPrint.orderInfoSenderContact+')^FS'+
				'^FO50,380^GB700,3,3^FS'+
				'^CFA,30'+
				'^FO50,420'+
				'^FB700,30,0,L'+
				'^FD'+dataPrint.orderInfoReceiverAddress+'^FS'+
				'^BY5,2,200'+
				'^FO250,550^BC^FD'+dataPrint.id+'^FS'+
				'^XZ';

		setVisible(false);
		fetch('http://'+ipThermalPrinter+'/service/printercommand.lua?command='+encodeURIComponent(cmd)+'&pageid=Services&pagename=Send+Printer+Commands')
		.then(res => {
			setDisablePrint(false);
		})
		.catch(error => {
			setDisablePrint(false);
			showErrorPrinter(error);
		});
	}

	const activateScan = () => {
		setScan(true);
	}

	const deactivateScan = () => {
		setScan(false);
	}

	const FlashOn = () => {
		setFlashButton(true);
		setTorchLight(RNCamera.Constants.FlashMode.torch);
	}

	const FlashOff = () => {
		setFlashButton(false);
		setTorchLight(RNCamera.Constants.FlashMode.off);
	}

	const getOrderInfoId = (barcodeCode) => {
		var stat = null;
		var orderInfoId = null;
		var totalPackInfo = null;
		var totalOrderPackingList = null;
		for (var h = 0; h < orderInfo.length; h++) {
			const packInfo = JSON.parse(orderInfo[h].packingInfo);
			for (var i = 0; i < packInfo.length; i++) {
				if (barcodeCode == packInfo[i].orderPackingInfoId) {
					orderInfoId = orderInfo[h].orderInfoId;
					totalPackInfo = packInfo[i].total;
					totalOrderPackingList = packInfo[i].totalOrderPackingList;
					stat = 200;
					break;
				} else {
					stat = 404;
				}
			}
			if (stat == 200) {
				break;
			}
		}

		return {
			orderInfoId: orderInfoId,
			totalPackInfo: totalPackInfo,
			totalOrderPackingList: totalOrderPackingList
		};
	}

	const cameraValidateSchema = Yup.object().shape({
		productId: Yup.string().required(),
		quantity: Yup.string().required(),
		orderItemId: Yup.string().required(),
		orderInfoId: Yup.string().required(),
	});

	const scanningReadBarcodeData = (scanResult, propsFormik) => {
		if (scanResult.data != null) {
			const barcodeCode = scanResult.data;
			if (barcodeCode == firstArray.productId && scans) {
				propsFormik.setFieldValue("productId", barcodeCode);
				setPrevProductId(barcodeCode);
			}

			const { productId, orderInfoId, orderPackInfo } = propsFormik.values;

			if (orderInfo.length === 1) {
				const single_orderInfoId = orderInfo[0].orderInfoId;
				const arrsingle_packingInfo = JSON.parse(orderInfo[0].packingInfo);
				const resArrSingle_packInfo = arrsingle_packingInfo.filter(d => {
					return d.total > d.totalOrderPackingList;
				});

				if (productId !== null && single_orderInfoId == firstArray.orderInfoId) {
					propsFormik.setFieldValue("orderPackInfo", resArrSingle_packInfo[0].orderPackingInfoId);
					propsFormik.setFieldValue("quantity", firstArray.quantity);
					propsFormik.setFieldValue("orderItemId", firstArray.orderItemId);
					propsFormik.setFieldValue("orderInfoId", firstArray.orderInfoId);
				}
			} else {
				if (productId !== null) {
					const data = getOrderInfoId(barcodeCode);
					setOrderPackInfo(barcodeCode);
					if (data.orderInfoId == firstArray.orderInfoId) {
						propsFormik.setFieldValue("orderPackInfo", barcodeCode);
						propsFormik.setFieldValue("quantity", firstArray.quantity);
						propsFormik.setFieldValue("orderItemId", firstArray.orderItemId);
						propsFormik.setFieldValue("orderInfoId", firstArray.orderInfoId);
					}
				}
			}

			if (productId !== null && orderInfoId !== null && orderPackInfo !== null && scans) {
				RNBeep.PlaySysSound(RNBeep.AndroidSoundIDs.TONE_CDMA_ANSWER);
				setScans(false);
				propsFormik.submitForm();
			}
		}
	}

	const doSubmitData = (values, formikBag) => {
		const payload = {
			id: values.orderPackInfo,
			orderInfoId: values.orderInfoId,
			quantity: values.quantity,
			productId: values.productId,
			orderItemId: values.orderItemId
		}

		dispatch(scanOrderPackingItemNew(payload)).then((res) => {
			if (res.success !== undefined && res.success === "FINISH") {
				setScan(false);
				Alert.alert("Yeaayy..!", "This order packaging info has been finished...!");
			} else if (res.failed !== undefined) {
				const failed = res.failed;
				Alert.alert("Owh No..!", failed);
			}
			
			formikBag.resetForm();

			if (orderInfo !== undefined && orderInfo.length > 0) {
				const payloadSession = { sessionId: sessionId }
				dispatch(orderPackingViewAllOrderInfoId(payloadSession));
				dispatch(orderPackingViewAllPackingList(payloadSession));
				dispatch(orderPackingViewAllItemScan(payloadSession));
			}
			setScans(true);
			setPrevProductId(null);
		});
	}

	const generatePacking = async () => {
		setLoadingGenerate(true);
		const strg_locationId = await AsyncStorage.getItem('@locationId');
		const payload = {
			locationId: locationId === undefined ? strg_locationId : locationId,
		}

		dispatch(getDataOrderPackingInfo(payload))
		.then(res => {
			if (res.response !== undefined && res.response.failed !== undefined) {
				showAlertError(res.response.failed);
			}

			setLoadingGenerate(false);
		})
		.catch(error => console.log(error));
	}

	const printAllDataOrderPack = async () => {
		setLoadingPrintAll(true);
		var ipThermalPrinter = await getThermalPrinterIp();
		if (orderInfo.length > 0) {
			var arrData = new Array();
			orderInfo.map(orderinfo => {
				const packInfo = JSON.parse(orderinfo.packingInfo);
				packInfo.map(j => {
					arrData.push(j.orderPackingInfoId);
				});
			});

			if (arrData.length > 0) {
				dispatch(getMultipleDataForPrint({
					id: arrData
				})).then(dataArr => {
					var cmd = '';
					for (var i = 0; i < dataArr.length; i++) {
						cmd += '^XA'+
							'^CF0,40'+
							'^FO50,100^FD'+dataArr[i].shippingName+'^FS'+
							'^FO50,150^GB700,3,3^FS'+
							'^CFA,30'+
							'^FO50,200^FDOR-ID: '+dataArr[i].orderInfoId+'^FS'+
							'^FO50,240^FDPK-ID: '+dataArr[i].id+'^FS'+
							'^FO50,300^FD'+dataArr[i].orderInfoReceiverName+' ('+dataArr[i].orderInfoReceiverContact+')^FS'+
							'^FO50,340^FDSender : '+dataArr[i].orderInfoSenderName+' ('+dataArr[i].orderInfoSenderContact+')^FS'+
							'^FO50,380^GB700,3,3^FS'+
							'^CFA,30'+
							'^FO50,420'+
							'^FB700,30,0,L'+
							'^FD'+dataArr[i].orderInfoReceiverAddress+'^FS'+
							'^BY5,2,200'+
							'^FO250,550^BC^FD'+dataArr[i].id+'^FS'+
							'^XZ';
					}

					fetch('http://'+ipThermalPrinter+'/service/printercommand.lua?command='+encodeURIComponent(cmd)+'&pageid=Services&pagename=Send+Printer+Commands')
					.catch((error) => {
						showErrorPrinter(error);
					});
				});

				setLoadingPrintAll(false);
				hideDialogAlert();
			}
		}
	}

	const showAlertError = (error) => {
		setDataAlertError(error);
		setAlertError(true);
	}

	const dismissAlertError = () => setAlertError(false);

	const showErrorPrinter = (error) => {
		setErrPrinter(error);
		setVisibleErrPrinter(true);
	}

	const dismissErrPrinter = () => setVisibleErrPrinter(false);

	const showInfoForPrint = (orderPackingInfoId) => {
		setVisible(true);
		setLoadingInfoDataPrint(true);
		dispatch(getSingleDataForPrint(orderPackingInfoId))
		.then(res => {
			setLoadingInfoDataPrint(false);
		})
		.catch(error => {
			console.log(error);
		});
	}

	const hideDialog = () => setVisible(false);
	const hideDialogAlert = () => setVisibleAlert(false);

	const handlerFinishButton = (orderPackInfoId) => {
		Alert.alert("Are you sure..?", "apakah anda yakin untuk mem-finish order info ini..?", [
			{
				text: "Cancel"
			},
			{
				text: "Yes",
				onPress: async () => {
					const payloadOrderPackFinish = { packingInfoId: orderPackInfoId }
					const response = await dispatch(orderPackingFinish(payloadOrderPackFinish));
					const payloadSession = { sessionId: sessionId }
					dispatch(orderPackingViewAllOrderInfoId(payloadSession));
					dispatch(orderPackingViewAllPackingList(payloadSession));
					dispatch(orderPackingViewAllItemScan(payloadSession));
					console.log(response);
					if (response.response.success !== undefined) {
						const message = response.response.success;
						Alert.alert("Success..!", message);
					} else if (response.response.failed !== undefined) {
						const message = response.response.failed;
						Alert.alert("Failed..!", message);
					}
				}
			}
		]);
	}

    return (
		<View style={styles.container}>
			<Appbar.Header style={styles.appHeader}>
				<Appbar.BackAction onPress={() => {
					navigation.goBack();
					deactivateScan();
				}} />
				<Appbar.Content title="Packing List" subtitle={'Order'} />
				<Appbar.Action
					icon="refresh"
					onPress={generatePacking}
				/>
			</Appbar.Header>
			<SafeAreaView style={styles.container}>
				{
					scan && navigation.isFocused() ? (
						<Formik
							validationSchema={cameraValidateSchema}
							initialValues={{
								productId: null,
								orderPackInfo: null,
								quantity: null,
								orderItemId: null,
								orderInfoId: null,
							}}
							onSubmit={(values,formikBag) => {
								formikBag.setSubmitting(true);
								doSubmitData(values, formikBag);
								formikBag.setSubmitting(false);
							}}
							enableReinitialize={true}>
							{props => (
								<RNCamera
									ref={ref => {
										setCamera(ref);
									}}
									defaultTouchToFocus
									flashMode={torchLight}
									mirrorImage={false}
									onBarCodeRead={(param) => {
										scanningReadBarcodeData(param, props);
									}}
									style={[styles.sizeBoxCameraScan, styles.styleBoxCameraScan]}
									ration={'1:1'}
									type={typeCamera}
									>
									<BarcodeMask width={400} height={150} edgeWidth={0} edgeHeight={0} />
									<View style={styles.buttonOnCamera}>
										{ flashButton && scan ? (
							        		<IconButton icon="flash-off" size={24} color="#000" style={styles.buttonFloatOnCameraBox} onPress={FlashOff} />
							        	) : (
							        		<IconButton icon="flash-outline" size={24} color="#000" style={styles.buttonFloatOnCameraBox} onPress={FlashOn} />
							        	) }

							        	<IconButton icon="barcode-off" size={24} color="#000" style={styles.buttonFloatOnCameraBox} onPress={deactivateScan} />
									</View>
								</RNCamera>
							)}
						</Formik>
					) : (
						<View style={[styles.sizeBoxCameraScan, styles.styleBoxCameraScan]}>
							{ dtPackingList !== undefined && dtPackingList.length > 0 && orderInfo.length > 0 ? (
									<Button icon="barcode" mode="outline" color="#000" style={styles.buttonOpenCameraScan} onPress={activateScan}>Open Scanner</Button>
								) : (
									<Button icon="play-outline" mode="outline" color="#000" loading={loadingGenerate} style={styles.buttonOpenCameraScan} onPress={() => {
										generatePacking();
									}}>Generate Packing</Button>
								)
							}
	  					</View>
					)
				}
				<View style={[styles.containerOrderPacking, styles.styleboxOrderInfo]}>
					<View style={{ padding: 5, justifyContent: 'space-around', flexDirection: 'row' }}>
						{
							orderInfo !== undefined && orderInfo.length > 0 ? (
								orderInfo.map((dataPack, key) => {
									const packingInfo = JSON.parse(dataPack.packingInfo);
									var border = false;
									if (firstArray) {
										if (firstArray.orderInfoId == dataPack.orderInfoId) {
				        					border = true;
				        				}
									}

									return (
										<View key={key} style={{
											alignItems: 'center',
											justifyContent: 'center'
										}}>
											<Text>{ dataPack.orderInfoId }</Text>
											<View style={styles.listOrderPackStyle}>
												<ScrollView
													contentContainerStyle={styles.contentContainerStyle}
							        				style={{
							        					textAlign: 'center',
														borderWidth: border ? 2 : 1,
														borderColor: border ? '#1EB206' : '#000',
														borderRadius: 8
							        				}}>
													{
								        				packingInfo.reverse().map((packInfo, keyIndex) => {
								        					return (
									        					<TouchableOpacity key={keyIndex} onPress={() => showInfoForPrint(packInfo.orderPackingInfoId)}>
									        						<Text style={ styles.textInScrollView }>
									        							<Subheading style={ styles.textSubHeading }>{packInfo.orderPackingInfoId}</Subheading>
									        							{"\n"}
									        							{packInfo.total + '/' + packInfo.totalOrderPackingList}
									        						</Text>
									        					</TouchableOpacity>
								        					)
								        				})
								        			}
												</ScrollView>
											</View>
											<SafeAreaView style={{ flexDirection: 'row', paddingTop: 0 }}>
								        		{
								        			packingInfo.map((packInfo, keyIndex) => {
								        				if (packInfo.totalOrderPackingList != packInfo.total && packInfo.totalOrderPackingList < packInfo.total) {
									        				return (
												        		<IconButton key={keyIndex} icon="check-all" size={20} color="#110F0F" style={{ marginVertical: 0 }} onPress={ () => handlerFinishButton(packInfo.orderPackingInfoId) } />
									        				)
								        				}
								        			})
								        		}
							        		</SafeAreaView>
										</View>
									)
								})
							) : (
								<View style={{ alignItems: 'center', justifyContent: 'center' }}>
					        		<Text style={{ fontWeight: '100', color: 'lightgray', fontSize: 20, fontStyle: 'italic' }}>Data is empty</Text>
					        	</View>
							)
						}
					</View>
				</View>
				<View style={[styles.containerFinishPacking, styles.styleBoxPackingList]}>
					<Card style={{ flex: 1, backgroundColor: 'white' }}>
	        			<Card.Title titleStyle={{ fontSize: 16, color: 'black', marginBottom: 0, marginVertical: 1 }} style={{ minHeight: 20 }} title="Packing List"/>
	        			<Card.Content>
	        				<FlatList
							  	contentContainerStyle={{ paddingBottom: 50 }}
							  	data={dtPackingList}
							  	keyExtractor={(item, index) => index}
							  	renderItem={({ item, index }) => {
							  		return (
								    	<View style={{ marginVertical: 5 }}>
								    		<Text
								    			style={{
								    				color: prevProductId !== null && index === 0 ? '#2038F0' : '#000',
								    			}}
								    		>
								    			<Text>
								    				{
								    					(index+1) +
									    				'. ' +
									    				item.categoryDescription +
									    				' '
								    				}
								    			</Text>
								    			<Text style={{ fontWeight: 'bold' }}>{ item.code }</Text>
									    		<Text>
									    			{
									    				' -- ' +
									    				item.position +
									    				' -- ' +
									    				item.productId +
									    				' ( ' + item.quantity + ' ' + item.type + ' ) '
									    			}
									    		</Text>
								    		</Text>
								      	</View>
								  	)
							  	}}
							/>
	        			</Card.Content>
	        		</Card>
	        		<Card style={{ flex: 1, backgroundColor: 'white' }}>
	        			<Card.Title titleStyle={{ fontSize: 16, color: 'black', marginBottom: 0, marginVertical: 1 }} style={{ minHeight: 20 }} title="Finished Packing List"/>
	        			<Card.Content>
	        				<FlatList
							  	contentContainerStyle={{ paddingBottom: 30 }}
							  	data={dtPackingListFinish}
							  	keyExtractor={(item, index) => index}
							  	renderItem={({ item, index }) => {
							  		return (
								    	<View
								    		style={{ marginVertical: 5 }}
								    	>
								    		<Text>
								    			<Text>
								    				{
								    					(index+1) +
									    				'. ' +
									    				item.categoryDescription +
								    					' '
								    				}
								    			</Text>
								    			<Text style={{ fontWeight: 'bold' }}>{ item.code }</Text>
									    		<Text>
									    			{
									    				' -- ' +
									    				item.productId +
									    				' ' +
									    				' ( Qty : ' + item.quantity + ' ) '
									    			}
									    		</Text>
								    		</Text>
								      	</View>
								  	)
							  	}}
							/>
	        			</Card.Content>
	        		</Card>
				</View>

				<Portal>
					<Dialog visible={visible} onDismiss={hideDialog} style={{ backgroundColor: '#fff' }}>
						<Dialog.ScrollArea>
							<ScrollView>
								{
									loadingInfoDataPrint ? (
										<View style={{
											flexDirection: 'row',
											alignItems: 'center'
										}}>
											<ActivityIndicator
												size={ isIOS ? 'large' : 48 }
												color="#000"
												style={{
													marginRight: 16,
													padding: 10
												}}
											/>
											<Paragraph>Loading.....</Paragraph>
										</View>
									) : (
										<View>
											<Title style={styles.textBlack}>{ dataPrint && 'PK-ID : '+dataPrint.id }</Title>
											<DataTable>
												<DataTable.Row>
													<DataTable.Cell style={styles.flexKeyNameColumn}>{ dataPrint && 'OR-ID' }</DataTable.Cell>
													<DataTable.Cell style={styles.flexColumnSeparate}>{ dataPrint && ':' }</DataTable.Cell>
													<DataTable.Cell>{ dataPrint && dataPrint.orderInfoId }</DataTable.Cell>
												</DataTable.Row>
												<DataTable.Row>
													<DataTable.Cell style={styles.flexKeyNameColumn}>{ dataPrint && 'Nama Pengiriman' }</DataTable.Cell>
													<DataTable.Cell style={styles.flexColumnSeparate}>{ dataPrint && ':' }</DataTable.Cell>
													<DataTable.Cell>{ dataPrint && dataPrint.shippingName }</DataTable.Cell>
												</DataTable.Row>
												<DataTable.Row>
													<DataTable.Cell style={styles.flexKeyNameColumn}>{ dataPrint && 'Nama Penerima' }</DataTable.Cell>
													<DataTable.Cell style={styles.flexColumnSeparate}>{ dataPrint && ':' }</DataTable.Cell>
													<DataTable.Cell>{ dataPrint && dataPrint.orderInfoReceiverName }</DataTable.Cell>
												</DataTable.Row>
												<DataTable.Row>
													<DataTable.Cell style={styles.flexKeyNameColumn}>{ dataPrint && 'Kontak Penerima' }</DataTable.Cell>
													<DataTable.Cell style={styles.flexColumnSeparate}>{ dataPrint && ':' }</DataTable.Cell>
													<DataTable.Cell>{ dataPrint && dataPrint.orderInfoReceiverContact }</DataTable.Cell>
												</DataTable.Row>
												<Line/>
												<DataTable.Row>
													<DataTable.Cell style={styles.flexKeyNameColumn}>{ dataPrint && 'Nama Pengirim' }</DataTable.Cell>
													<DataTable.Cell style={styles.flexColumnSeparate}>{ dataPrint && ':' }</DataTable.Cell>
													<DataTable.Cell>{ dataPrint && dataPrint.orderInfoSenderName }</DataTable.Cell>
												</DataTable.Row>
												<DataTable.Row>
													<DataTable.Cell style={styles.flexKeyNameColumn}>{ dataPrint && 'Kontak Pengirim' }</DataTable.Cell>
													<DataTable.Cell style={styles.flexColumnSeparate}>{ dataPrint && ':' }</DataTable.Cell>
													<DataTable.Cell>{ dataPrint && dataPrint.orderInfoSenderContact }</DataTable.Cell>
												</DataTable.Row>
											</DataTable>
										</View>
									)
								}
							</ScrollView>
						</Dialog.ScrollArea>
						{
							!loadingInfoDataPrint &&
							<Dialog.Actions style={{
					        	justifyContent: 'space-between',
					        	marginLeft: 15,
					        	marginRight: 15,
					        }}>
								<Button icon="close" mode="outlined" onPress={hideDialog} labelStyle={{ color: '#000' }} />
								<Button
									icon="printer"
									mode="outlined"
									loading={disablePrint}
									disabled={disablePrint}
									onPress={printDataOrderPack}
									labelStyle={{
										color: '#000'
									}}
								/>
							</Dialog.Actions>
						}
					</Dialog>
				</Portal>

				<Portal>
			    	<Dialog visible={visibleAlert} onDismiss={hideDialogAlert} style={{ backgroundColor: '#fff' }}>
			    		<Dialog.Title style={{ color: '#000' }}>Label Print</Dialog.Title>
				        <Dialog.Content>
				        	<Paragraph style={{ color: '#000' }}>Apakah anda ingin mencetak semua Order Packing Info ?</Paragraph>
				        </Dialog.Content>
				        <Dialog.Actions style={{
				        	justifyContent: 'space-between'
				        }}>
							<Button icon="close" mode="outlined" onPress={hideDialogAlert} style={{ borderColor: '#000' }} labelStyle={{ color: '#000' }} />
							<Button
								icon="printer"
								mode="outlined"
								style={{ borderColor: '#000' }}
								onPress={printAllDataOrderPack}
								loading={loadingPrintAll}
								labelStyle={{
									color: '#000'
								}}
							/>
						</Dialog.Actions>
			    	</Dialog>
			    </Portal>
			</SafeAreaView>

			<SnackbarAlert visible={visibleErrPrinter} onDismiss={dismissErrPrinter} duration={3000} backgroundColor="#000" textColor="#FFFFFF" message={ "Can't connect to printer, please check web server printer\n"+errPrinter } setActionOnPress={() => setVisibleErrPrinter(false)} />
			<SnackbarAlert visible={alertError} onDismiss={dismissAlertError} duration={3000} backgroundColor="#000" textColor="#FFFFFF" message={dataAlertError} setActionOnPress={() => setAlertError(false)} />
		</View>
    );
}

export default PackingListScreenOld;
