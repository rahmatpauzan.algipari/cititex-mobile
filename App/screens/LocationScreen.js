import React, { useState, useEffect } from 'react';
import {
	View,
	Text,
	StyleSheet
} from 'react-native';
import { List } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-community/async-storage';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'flex-start',
		backgroundColor: 'white'
	}
});

const LocationScreen = ({ navigation }) => {
	const [location, setLocation] = useState([]);
	const [locationId, setLocationId] = useState([]);

	useEffect(async () => {
		await loadData();
	}, []);

	const loadData = async () => {
		const arrayLocation = await AsyncStorage.getItem('@location');
		const dt = JSON.parse(arrayLocation);
		setLocation(dt);
		
		const locationId = await AsyncStorage.getItem('@locationId');
		setLocationId(locationId);
	}

	return (
		<View style={styles.container}>
			<List.Section>
				<List.Subheader style={{ fontSize: 18, color: '#000', fontWeight: 'bold' }}>
					List of Location
				</List.Subheader>
				{
					location && location.map((locate, key) => (
							<List.Item
								key={key}
								title={locate.name}
								titleStyle={{
									color: 'black',
								}}
								description={locate.address}
								descriptionStyle={{
									color: 'black',
								}}
								style={{
									backgroundColor: 'white',
								}}
								onPress={async () => {
									const locId = await AsyncStorage.getItem('@locationId');
									if (locId != locate.id) {
										const keys = ['@listOrder', '@listBarang', '@listHistory']
										await AsyncStorage.multiRemove(keys);
									}
									await AsyncStorage.setItem('@locationId', JSON.stringify(locate.id));
									await loadData();
								}}
								left={(styles) => {
									if (locationId == locate.id) {
										return (
											<List.Icon style={{ color: styles.color }} color="#2E4FF5" icon="star" />
										)
									} else {
										return (
											<List.Icon style={{ color: styles.color }} icon="star-outline" />
										)
									}
								}}
							/>
						)
					)
				}
			</List.Section>
		</View>
	);
}

export default LocationScreen;