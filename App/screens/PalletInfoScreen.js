import React, { useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  Dimensions,
  Animated,
  FlatList,
  Alert,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import {
	Appbar,
	Button,
	IconButton,
	Subheading,
	Card,
	Title
} from 'react-native-paper';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import { RectButton } from 'react-native-gesture-handler';
import { useSelector, useDispatch } from 'react-redux';
import BarcodeMask from 'react-native-barcode-mask';
import { RNCamera } from 'react-native-camera';
import { Formik, useFormik } from 'formik';
import * as Yup from 'yup';

const _ = require("lodash");

const deviceWidth = Dimensions.get('screen').width;
const deviceHeight = Dimensions.get('screen').height;

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	centerContainer: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	appHeader: {
		backgroundColor: '#191919',
	},
	sizeBoxCameraScan: {
		flex: 1
	},
	styleBoxCameraScan: {
		justifyContent: 'center',
		alignItems: 'center',
		margin: 3
	},
	sizeBoxPalletInfoId: {
		flex: 1
	},
	sizeBoxBarcodeResult: {
		flex: 3
	},
	styleBoxBarcodeResult: {
		borderWidth: 1,
		borderColor: '#000',
		backgroundColor: '#fff',
		borderRadius: 5,
		margin: 3
	},
	styleStartDefault: {
		justifyContent: 'center',
		alignItems: 'center',
		borderWidth: 1,
		borderColor: '#000',
		backgroundColor: '#fff',
		borderRadius: 5,
		margin: 3
	},
	buttonOpenCameraScan: {
		borderWidth: 1,
		borderColor: '#000',
		fontSize: 21,
		fontWeight: 'bold'
	},
	buttonOnCamera: {
		position: 'absolute',
		top: 50,
		width: '100%',
		flexDirection: 'column',
		justifyContent: 'center'
	},
	buttonBarcodeOnCamera: {
		backgroundColor: 'white',
		borderWidth: 1,
		borderColor: '#000'
	},
	floatingButtonOnRightBottom: {
		position: 'absolute',
		bottom: 20,
		right: 20,
		width: 60,
		height: 60,
		borderWidth: 2,
		borderColor: '#fff',
		backgroundColor: '#000',
		borderRadius: 50
	},
	containerInsideSwipe: {
		paddingHorizontal: 10,
		paddingVertical: 15,
		alignItems: 'center'
	},
	rightAction: {
		backgroundColor: '#fff',
		justifyContent: 'center',
		alignItems: 'flex-end'
	},
	separator: {
		flex: 1,
		height: 1,
		backgroundColor: '#e4e4e4',
		marginLeft: 10
	}
});

const Separator = () => <View style={styles.separator} />;

const RenderRightActions = () => {
	return (
		<View style={styles.rightAction}>
			<IconButton icon="close-box" color="#dd2c00" size={25} onPress={() => Alert.alert("Alert", "Pressed Right Delete")} />
		</View>
	);
}

const PalletInfoScreen = ({ navigation }) => {
	const [camera, setCamera] = useState(null);
	const [scan, setScan] = useState(false);
	const [torchLight, setTorchLight] = useState(RNCamera.Constants.FlashMode.off);
	const [typeCamera, setTypeCamera] = useState(RNCamera.Constants.Type.back);

	const formik = useFormik({
		initialValues: {
			palletInfoId: null,
			itemInPallet: []
		},
		onSubmit: (values) => {
			// console.log('values', values);
		},
		enableReinitialize: false
	});

	const activateScan = () => {
		setScan(true);
	}

	const deactivateScan = () => {
		setScan(false);
	}

	const barcodeReadData = (scanResult) => {
		if (scanResult.data != null) {
			const dataBarcode = scanResult.data;
			formik.setSubmitting(true);
			const arr = formik.values.itemInPallet;
			if (arr.length > 0) {
				const check = arr.filter(d => { return d == dataBarcode });
				if (check.length === 0) {
					arr.push(dataBarcode);
				} else {
					console.log('ada isi nya yang sama');
				}
			} else {
				arr.push(dataBarcode);
			}
			
			formik.setFieldValue("itemInPallet", arr);
			formik.submitForm();
			deactivateScan();
		}
	}

    return (
      <View style={styles.container}>
      	<Appbar.Header style={styles.appHeader}>
      		<Appbar.BackAction onPress={() => {
      			navigation.goBack();
      			deactivateScan();
      		}} />
      		<Appbar.Content title="Pallet Info" />
      	</Appbar.Header>
      	<SafeAreaView style={styles.container}>
      		{ scan && navigation.isFocused() ? (
      			<RNCamera
					ref={ref => {
						setCamera(ref);
					}}
					defaultTouchToFocus
					flashMode={torchLight}
					mirrorImage={false}
					onBarCodeRead={(param) => {
						barcodeReadData(param);
					}}
					style={[styles.sizeBoxCameraScan, styles.styleBoxCameraScan]}
					ration={'1:1'}
					type={typeCamera}
				>
					<BarcodeMask width={400} height={150} edgeWidth={0} edgeHeight={0} />
					<View style={styles.buttonOnCamera}>
						<IconButton icon="barcode-off" size={24} color="#000" style={styles.buttonBarcodeOnCamera} onPress={deactivateScan} />
					</View>
				</RNCamera>
  				) : (
  					<View style={[styles.sizeBoxCameraScan, styles.styleBoxCameraScan]}>
  						<Button icon="barcode" mode="outline" color="#000" style={styles.buttonOpenCameraScan} onPress={activateScan}>Open Scan</Button>
  					</View>
  				)
  			}
      		<View style={[styles.sizeBoxPalletInfoId, styles.styleStartDefault]}>
      			<Title>Palet Info Id : { formik.values.palletInfoId }</Title>
      		</View>
      		<View style={[styles.sizeBoxBarcodeResult, styles.styleBoxBarcodeResult]}>
      			<Card style={{ flex: 1, backgroundColor: 'white' }}>
      				<Card.Title titleStyle={{ fontSize: 12 }} title="Barcode Result of Boxes" />
      				<Card.Content>
      					<FlatList
      						contentContainerStyle={{ paddingBottom: 50 }}
		      				data={formik.values.itemInPallet}
		      				keyExtractor={(item, index) => index}
		      				renderItem={({ item, index }) => (
			      				<ScrollView>
			      					<Swipeable
			      						renderRightActions={() => <RenderRightActions />}
			      					>
				      					<View style={styles.containerInsideSwipe}>
			      							<Text style={{ color: '#4a4a4a', fontSize: 17, fontWeight: 'bold' }}>{ item }</Text>
			      						</View>
			      					</Swipeable>
			      				</ScrollView>
		      				)}
		      			ItemSeparatorComponent={() => <Separator />}
		      			/>
      				</Card.Content>
      			</Card>
      		</View>
      		<IconButton icon="content-save-all-outline" color="#fff" style={styles.floatingButtonOnRightBottom} size={30} onPress={() => {}} />
      	</SafeAreaView>
      </View>
    );
}

export default PalletInfoScreen;
