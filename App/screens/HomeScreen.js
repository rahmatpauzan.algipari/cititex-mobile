import React from 'react';
import {
	View,
	Text,
	StyleSheet,
	Button
} from 'react-native';
import { useSelector } from 'react-redux';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	}
});

const HomeScreen = ({ navigation }) => {
	return (
		<View style={styles.container}>
			<Text>Home Page</Text>
			<Button title="Details" onPress={() => navigation.push('Details', { name: "Navigate from Home" })} />
			<Button title="Location" onPress={() => navigation.navigate('TabLocation')} />
		</View>
	);
}

export default HomeScreen;
