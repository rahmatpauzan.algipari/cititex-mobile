import React, { useRef, useState, useEffect } from 'react';
import {
  	View,
	ScrollView,
	Text,
	StyleSheet,
	TouchableOpacity,
	Alert,
	Platform,
	FlatList,
	VirtualizedList,
	TouchableHighlight,
	SafeAreaView,
	AppState
} from 'react-native';
import {
	Appbar,
	Button,
	IconButton,
	Subheading,
	Card,
	Title,
	Paragraph,
	Dialog,
	Portal,
	Snackbar,
	DataTable,
	ActivityIndicator,
	TouchableRipple
} from 'react-native-paper';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import Orientation from 'react-native-orientation-locker';
import Printer from 'react-native-epson-epos-printer';
import NetInfo from '@react-native-community/netinfo';
import Spinner from 'react-native-loading-spinner-overlay';

import { RNCamera } from 'react-native-camera';
import BarcodeMask from 'react-native-barcode-mask';
import { useSelector, useDispatch } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import AwesomeAlert from 'react-native-awesome-alerts';
import RNBeep from 'react-native-a-beep';
import { useFocusEffect } from '@react-navigation/native';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { Formik } from 'formik';
import * as Yup from 'yup';
import { getAxisLocation, getAxisCenterHeaderBarcode, getDataLocation, getRoles } from '../config/Utils';

import SnackbarAlert from '../components/SnackbarAlert';
import BarcodeForm from '../components/BarcodeForm';
import SwipeProvider from '../components/swipe/SwipeProvider';
import SwipeableRow from '../components/swipe/SwipeableRow';

import {
	getDataOrderPackingInfo,
	scanOrderPackingItemNew,
	orderPackingViewAllOrderInfoId,
	orderPackingViewAllPackingList,
	orderPackingViewAllItemScan,
	orderPackingFinish,
	getSingleDataForPrint,
	getMultipleDataForPrint,
	getResultCheckOrderpacking,
	deleteDataHistoryPackingList,
	cancelFetch
} from '../redux/actions';

const _ = require("lodash");

const isIOS = Platform.OS === 'ios';

const coordinat = [
	{
		first: 555,
		second: 573,
		third: 593
	},
	{
		first: 600,
		second: 618,
		third: 638
	},
	{
		first: 648,
		second: 666,
		third: 686
	},
	{
		first: 696,
		second: 714,
		third: 734
	},
	{
		first: 744,
		second: 762,
		third: 782
	},
	{
		first: 792,
		second: 810,
		third: 830
	},
	{
		first: 840,
		second: 858,
		third: 878
	},
	{
		first: 888,
		second: 906,
		third: 926
	},
	{
		first: 936,
		second: 954,
		third: 974
	},
	{
		first: 984,
		second: 1002,
		third: 1022
	},
	{
		first: 1032,
		second: 1050,
		third: 1070
	},
	{
		first: 1080,
		second: 1098,
		third: 1118
	},
	{
		first: 1128,
		second: 1146,
		third: 1166
	},
	{
		first: 1176,
		second: 1194,
		third: 1214
	},
	{
		first: 1224,
		second: 1242,
		third: 1262
	}
];

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	appHeader: {
		backgroundColor: '#191919',
	},
	sizeBoxCameraScan: {
		flex: 1
	},
	styleBoxCameraScan: {
		justifyContent: 'center',
		alignItems: 'center',
		margin: 3
	},
  	styleboxOrderInfo: {
		backgroundColor: '#fff'
	},
	styleBoxPackingList: {
		backgroundColor: '#fff'
	},
  	buttonTextStyle: {
        color: 'white',
        fontWeight: 'bold',
    },
    buttonFlash: {
    	borderWidth: 1,
    	borderColor: '#000'
    },
    buttonClose: {
    	borderWidth: 1,
    	borderColor: '#000',
    	width: '30%'
    },
    buttonOpenCameraScan: {
		borderWidth: 1,
		borderColor: '#000',
		fontSize: 21,
		fontWeight: 'bold'
	},
	buttonOnLeftCamera: {
		position: 'absolute',
		top: hp('4%'),
		left: 0,
		flexDirection: 'column',
		justifyContent: 'center'
	},
	buttonOnRightCamera: {
		position: 'absolute',
		top: hp('4%'),
		right: 0,
		flexDirection: 'column',
		justifyContent: 'center'
	},
	buttonFloatOnCameraBox: {
		backgroundColor: 'white',
		borderWidth: 1,
		borderColor: '#fff'
	},
    containerButton: {
    	flex: 0.3,
    	borderWidth: 1,
	    flexDirection: 'row',
	    justifyContent: 'space-evenly',
	    alignItems: 'center'
	},
	containerOrderPacking: {
		// flex: 0.8,
		height: 'auto',
		width: 'auto'
	},
	containerOrderPackStyle: {
		flexDirection: 'row',
		padding: 3
	},
	listOrderPackStyle: {
		height: hp('7%'),
		width: wp('30%'),
		padding: 2
	},
	contentContainerStyle: {
		alignItems: 'center',
		paddingTop: 0,
		paddingBottom: 7,
		width: wp('28.5%')
	},
	scrollViewStyle: {
		textAlign: 'center',
		borderWidth: 1,
		borderColor: '#000',
		borderRadius: 8
	},
	textSubHeading: {
		fontSize: 12,
		color: 'black',
	},
	textInScrollView: {
		fontSize: 20,
		textAlign: 'center',
	},
	containerFinishPacking: {
		flex: 3
	},
	textBlack: {
		color: '#000',
		paddingHorizontal: 16
	},
	flexKeyNameColumn: {
		flexBasis: 110,
		flexShrink: 1,
		flexGrow: 0
	},
	flexColumnSeparate: {
		flexBasis: 20,
		flexShrink: 1,
		flexGrow: 0
	}
});

const Line = () => (
	<View style={{ height: 1.5, backgroundColor: '#000', alignSelf: 'stretch' }} />
)

const longBarcodeResult = 26;

const ltrim = (str, char) => {
	const rgxtrim = (!char) ? new RegExp('^\\s+') : new RegExp('^'+char+'+');
	return str.replace(rgxtrim, '');
}

const PackingListScreen = ({ navigation }) => {
	const dispatch = useDispatch();

	const arrayLocation = useSelector(state => state.login.data.location);
	const { orderInfo, packingList, responseOrderPackNew, packingListItem, sessionId } = useSelector(state => state.packingList);

	const dtPackingList =  _.orderBy(packingList, ['position'], ['asc']);
	const firstArray = _.first(dtPackingList);

	const [abortConst, setAbortConst] = useState(new AbortController());
	const abortController = abortConst;

	const [scan, setScan] = useState(false);
	const [scans, setScans] = useState(true);
	const [camera, setCamera] = useState(null);
	const [codeScan, setCodeScan] = useState(false);
	const [scanResult, setScanResult] = useState(false);
	const [flashButton, setFlashButton] = useState(false);
	const [orderInfoId, setOrderInfoId] = useState(null);
	const [totalPackInfo, setTotalPackInfo] = useState(null);
	const [prevProductId, setPrevProductId] = useState(null);
	const [totalOrderPackingList, setTotalOrderPackingList] = useState(null);
	const [typeCamera, setTypeCamera] = useState(RNCamera.Constants.Type.back);
	const [torchLight, setTorchLight] = useState(RNCamera.Constants.FlashMode.off);
	const [visible, setVisible] = useState(false);
	const [visibleAlert, setVisibleAlert] = useState(false);

	const [alertError, setAlertError] = useState(false);
	const [dataAlertError, setDataAlertError] = useState(null);
	const [colorAlert, setColorAlert] = useState('#000');

	const [visibleErrPrinter, setVisibleErrPrinter] = useState(false);
	const [errPrinter, setErrPrinter] = useState(null);

	const [loadingPrintAll, setLoadingPrintAll] = useState(false);
	const [loadingPrintPackingList, setLoadingPrintPackingList] = useState(false);
	const [loadingPrintAlamat, setLoadingPrintAlamat] = useState(false);
	const [loadingScanNew, setLoadingScanNew] = useState(false);

	const [visibleBcForm, setVisibleBcForm] = useState(false);

	const [loadingGenerate, setLoadingGenerate] = useState(false);
	const [loadingRefresh, setLoadingRefresh] = useState(false);

	const [dtListOrder, setDtListOrder] = useState([]);
	const [dtListBarang, setDtListBarang] = useState([]);
	const [dtHistory, setDtHistory] = useState([]);

	const [dtDetailPrint, setDtDetailPrint] = useState([]);

	const [dtOrderPackingInfoId, setDtOrderPackingInfoId] = useState('');
	const [dtOrderInfoId, setDtOrderInfoId] = useState('');

	const listBarang =  _.orderBy(dtListBarang, ['position'], ['asc']);
	const lstbrg_firstArray = _.first(listBarang);

	const dtPackingListFinish = _.orderBy(dtHistory, ['position'], ['asc']);

	useFocusEffect(
		React.useCallback(() => {
			setDataAlertError(null);
			startingGenerate();
			asyncDataStorage();
			Orientation.lockToPortrait();
		}, [])
	);

	const asyncDataStorage = async () => {
		try {
			const listOrd = await AsyncStorage.getItem('@listOrder');
			const listBrg = await AsyncStorage.getItem('@listBarang');
			const listHstry = await AsyncStorage.getItem('@listHistory');
			setDtListOrder(JSON.parse(listOrd));
			setDtListBarang(JSON.parse(listBrg));
			setDtHistory(JSON.parse(listHstry));
		} catch(e) {
			console.log(e);
		}
	}

	const startingGenerate = async () => {
		const roles = await getRoles();
		const role_view_ordpkg = 'orderpacking_view';
		const orderpacking_view = roles.includes(role_view_ordpkg);
		const resultLocate = await AsyncStorage.getItem('@locationId');
		const locateId = JSON.parse(resultLocate);

		if (orderpacking_view) {
			dispatch(getResultCheckOrderpacking({ locationId: locateId }))
			.then(async (resCheck) => {
				if (resCheck.response !== undefined && resCheck.response.success !== undefined) {
					const message = resCheck.response.success;
					await showAlertError(message);
					await AsyncStorage.setItem('@listAllHistory', JSON.stringify([]));
					dispatch(getDataOrderPackingInfo({ locationId: locateId })).then(async (valres) => {
						if (Array.isArray(valres.orderInfoId) && Array.isArray(valres.packingList) && Array.isArray(valres.packingListItem)) {

							await AsyncStorage.setItem('@listOrder', JSON.stringify(valres.orderInfoId));
							await AsyncStorage.setItem('@listBarang', JSON.stringify(valres.packingList));
							await AsyncStorage.setItem('@listHistory', JSON.stringify(valres.packingListItem.reverse()));
							await AsyncStorage.setItem('@sessionId', JSON.stringify(valres.sessionId));

							const latest = await AsyncStorage.getItem('@listAllHistory');
							const latestListHistory = JSON.parse(latest);
							if (latestListHistory.length != valres.packingListItem.length) {
								await AsyncStorage.setItem('@listAllHistory', JSON.stringify(valres.packingListItem.reverse()));
							}
							asyncDataStorage();
						} else {
							await AsyncStorage.setItem('@listOrder', JSON.stringify([]));
							await AsyncStorage.setItem('@listBarang', JSON.stringify([]));
							await AsyncStorage.setItem('@listHistory', JSON.stringify([]));
							asyncDataStorage();
							if (valres.response != undefined) {
								showAlertError(valres.response.failed);
							}
						}
					})
					.catch(error => {
						Alert.alert("Sorry, something when wrong.", error.message, [
						{
							text: "Try again",
							onPress: () => {
								startingGenerate();
							}
						}
						]);
					});
				} else if (resCheck.response.failed !== undefined) {
					const dt = await getDataLocation();
					const dtsession = await AsyncStorage.getItem('@sessionId');

					const payloadSession = { sessionId: JSON.parse(dtsession), locationId: dt.locationId }
					const lstOrd = await dispatch(orderPackingViewAllOrderInfoId(payloadSession));
					const lstBrg = await dispatch(orderPackingViewAllPackingList(payloadSession));
					const lstHstry = await dispatch(orderPackingViewAllItemScan(payloadSession));

					if ((Array.isArray(lstOrd) && lstOrd.length > 0) && (Array.isArray(lstBrg) && lstBrg.length > 0) || (Array.isArray(lstHstry) && lstHstry.length > 0)) {
						await AsyncStorage.setItem('@listOrder', JSON.stringify(lstOrd));
						await AsyncStorage.setItem('@listBarang', JSON.stringify(lstBrg));
						await AsyncStorage.setItem('@listHistory', JSON.stringify(lstHstry.reverse()));
						asyncDataStorage();
					} else {
						const first = ['@listOrder', JSON.stringify([])];
						const second = ['@listBarang', JSON.stringify([])];
						const third = ['@listHistory', JSON.stringify([])];
						await AsyncStorage.multiSet([first, second, third]);
						asyncDataStorage();
						const message = "Anda tidak memiliki order packing untuk dihandle, silahkan generate..!";
						showAlertError(message);
					}
				}
			})
			.catch(error => {
				Alert.alert("Sorry, something when wrong.", error.message, [
				{
					text: "Try again",
					onPress: () => {
						startingGenerate();
					}
				}
				]);
			});
		} else {
			const colorMessage = '#F89E00';
    		const message = "You doesn't have permission to access view data orderpacking..!";
			showAlertError(message, colorMessage);
		}
	}

	const printPackingList = async () => {
		setLoadingPrintPackingList(true);
		const dt = await getDataLocation();
		var ipEpsonPrinter = dt.ipEpsonPrinter;

		if (ipEpsonPrinter == null || ipEpsonPrinter == '') {
			const colorMessage = '#F89E00';
			const message = "Silahkan tetapkan nilai IP Printer Epson terlebih dahulu...!";
			showAlertError(message, colorMessage);
			setVisible(false);
			setLoadingPrintPackingList(false);
		} else {
			if (dtOrderPackingInfoId != '') {
				const content1 = [
					{
						"text": "===============================================",
					},
					{
						"text": "Packing ID : "+dtOrderPackingInfoId,
					},
					{
						"text": "Order ID   : "+dtDetailPrint[0].orderInfoId,
					},
					{
						"text": "Order Time : "+dtDetailPrint[0].addTime.slice(0, 10)+" "+dtDetailPrint[0].addTime.slice(11, 19),
					},
					{
						"text": "Staff      : "+dtDetailPrint[0].submit,
					},
					{
						"text": "Sender     : "+dtDetailPrint[0].senderName+" ("+dtDetailPrint[0].senderContact+")",
					},
					{
						"text": "Receiver   : "+dtDetailPrint[0].receiverName+" ("+dtDetailPrint[0].receiverContact+")",
					},
					{
						"text": dtDetailPrint[0].receiverAddress+", "+dtDetailPrint[0].kelurahanName+", "+dtDetailPrint[0].kotaName+", "+dtDetailPrint[0].provinsiName+", "+dtDetailPrint[0].postalCode.toString(),
					},
					{
						"text": "===============================================",
					}
				]

				const resAllHistory = await AsyncStorage.getItem('@listAllHistory');
				const listAllHistories = JSON.parse(resAllHistory);

				const dtListHistory = dtPackingListFinish.length > 0 ? _.filter(dtPackingListFinish, {'orderInfoId': dtOrderInfoId, 'orderPackingInfoId': dtOrderPackingInfoId}) : _.filter(listAllHistories, {'orderInfoId': dtOrderInfoId, 'orderPackingInfoId': dtOrderPackingInfoId});

				var qty = 0;
				const content2 = await dtListHistory.map(item => {
					qty += parseInt(item.quantity);
					const dt = {
						"text": "\u001BE\u0001"+item.quantity+"\u001BE\0  " + item.categoryDescription + " - " + item.productDescription + "\n" + "-------------------------------",
					}

					return dt;
				});

				const breakLine = [
					{
						"text": "===============================================",
					}
				]

				const footer = [
					{
						"text": "Total Qty  : " + qty + " PCS"
					},
					{
						"text": "Shipping   : " + dtDetailPrint[0].shippingName
					},
					{
						"barcode": dtOrderPackingInfoId,
						"type": 6,
						"width": 2,
						"height": 75,
						"HRI": "HRI_NONE"
					}
				];

				const result = content1.concat(content2).concat(breakLine).concat(footer);
				Printer.print(1, ipEpsonPrinter, JSON.stringify(result));
				setLoadingPrintPackingList(false);
			} else {
				setLoadingPrintPackingList(false);
				Alert.alert("Sorry..!", "can't found order packing info ID");
			}
		}
	}

	const printAlamat = async () => {
		setLoadingPrintAlamat(true);
		const dt = await getDataLocation();
		var ipThermalPrinter = dt.ipThermalPrinter;

		if (ipThermalPrinter == null || ipThermalPrinter == '') {
			const colorMessage = '#F89E00';
			const message = "Silahkan tetapkan nilai IP Printer Thermal terlebih dahulu...!";
			showAlertError(message, colorMessage);
			setVisible(false);
			setLoadingPrintAlamat(false);
		} else {
			if (dtOrderPackingInfoId != '') {
				const idData = dtOrderPackingInfoId.toString();
				const X = getAxisCenterHeaderBarcode(idData);
				if (Array.isArray(dtListOrder) && dtListOrder.length > 0) {
					for(var lo = 0; lo < dtListOrder.length; lo++){
						if (dtListOrder[lo].orderInfoId == dtOrderInfoId) {
							const receiverName = dtListOrder[lo].receiverName;
							const orderInfoReceiverAddress = dtListOrder[lo].receiverAddress.replace(/\s+/g, ' ');
							const kotaName = dtListOrder[lo].kotaName ? ', '+dtListOrder[lo].kotaName : null;
							const kelurahanName = dtListOrder[lo].kelurahanName ? ', '+dtListOrder[lo].kelurahanName : null;
							const provinsiName = dtListOrder[lo].provinsiName ? ', '+dtListOrder[lo].provinsiName : null;
							const postalCode = dtListOrder[lo].postalCode ? ', '+dtListOrder[lo].postalCode : null;
							const receiverContact = dtListOrder[lo].receiverContact;
							const receiverAddress = orderInfoReceiverAddress+kelurahanName+kotaName+provinsiName+postalCode;
							const shippingName = dtListOrder[lo].shippingName;
							const orderInfoSenderName = dtListOrder[lo].senderName;
							const orderInfoSenderContact = dtListOrder[lo].senderContact;

							const lengthReceiverName = receiverName.length;
							const lengthOrderInfoSenderName = orderInfoSenderName.length;
							const lengthReceiverContact = receiverContact.length;
							const totalLengthReceiverName = parseInt(lengthReceiverName);
							const totalLengthOrderInfoSenderName = parseInt(lengthOrderInfoSenderName);

							/**
							 * zpl code for whitespace in hex UTF-8 is C2 A0
							 * use ^FH with indicator _ (underscore is default) to ^FD
							 * example : ^FO50,100^A0N,51,51^FH^FDexample_C2_A0text
							*/

							var cmdCondition, bookingCode, lineX_bookingCode, lineY_bookingCode;
							var lineY_horizontalLineFirst, lineY_horizontalLineSecond, lineY_horizontalLineThird, lineY_horizontalLineForth, lineY_horizontalLineFifth, extraAxisY1, extraAxisY2;
							var lengthVerticalLineFirst, lengthVerticalLineSecond, lengthVerticalLineThird;
							var startAxisY, lineY_titleOrderId, lineY_verticalLineFirst, lineY_orderId, lineY_date, lineY_verticalLineSecond, lineY_titleCourier, lineY_titlePackingId, lineY_CourierName, lineY_packingId, lineY_verticalLineThird, lineY_titleSender, lineY_titleReceiver, lineY_senderName, lineY_senderContact, lineY_receiverContact, lineY_receiverAddress;
							var lineY_dtpOrdPackItem_quantity, lineY_dtpOrdPackItem_categoryName, lineY_dtpOrdPackItem_productDescription;
							var lineY_next_dtpOrdPackItem_quantity, lineY_next_dtpOrdPackItem_categoryName, lineY_next_dtpOrdPackItem_productDescription;

							if (totalLengthOrderInfoSenderName > 42 && totalLengthOrderInfoSenderName <= 95) {
								extraAxisY1 = 25;
							} else if (totalLengthOrderInfoSenderName > 95) {
								extraAxisY1 = 50;
							} else {
								extraAxisY1 = 0;
							}

							if (totalLengthReceiverName > 42 && totalLengthReceiverName <= 95) {
								extraAxisY2 = 20;
							} else if (totalLengthReceiverName > 95) {
								extraAxisY2 = 50;
							} else {
								extraAxisY2 = 0;
							}

							startAxisY = 10;
							lineY_titleOrderId = startAxisY + 10; // 20
							lineY_verticalLineFirst = startAxisY;
							lineY_orderId = lineY_titleOrderId + 25; // 45
							lineY_date = lineY_orderId + 25; // 70

							lengthVerticalLineFirst = 85;
							lengthVerticalLineSecond = 65;
							lengthVerticalLineThird = 105;

							lineY_horizontalLineFirst = startAxisY + lengthVerticalLineFirst + 5; // 100

							lineY_verticalLineSecond = lineY_horizontalLineFirst + 10; // 110
							lineY_titleCourier = lineY_horizontalLineFirst + 15; // 115
							lineY_titlePackingId = lineY_horizontalLineFirst + 15; // 115
							lineY_CourierName = lineY_titleCourier + 30; // 145
							lineY_packingId = lineY_titlePackingId + 30; // 145

							lineY_horizontalLineSecond = lineY_verticalLineSecond + lengthVerticalLineSecond + 5; // 180

							lineY_verticalLineThird = lineY_horizontalLineSecond + 10; // 190
							lineY_titleSender = lineY_horizontalLineSecond + 15; // 195
							lineY_titleReceiver = lineY_horizontalLineSecond + 15; // 195
							lineY_senderName = lineY_titleSender + 25; // 220
							lineY_senderContact = lineY_senderName + 25 + extraAxisY1; // 245
							lineY_receiverContact = lineY_titleReceiver + 25 + extraAxisY2; // 220 / 240 / 270

							lineY_horizontalLineThird = lineY_verticalLineThird + lengthVerticalLineThird + 5; // 300

							lineY_receiverAddress = lineY_horizontalLineThird + 10; // 310

							lineY_horizontalLineForth = lineY_receiverAddress + 110; // 420

							if (dtListOrder[lo].bookingCode) {
								bookingCode = dtListOrder[lo].bookingCode;
								lineX_bookingCode = getAxisLocation(bookingCode);
								lineY_bookingCode = lineY_horizontalLineForth + 10; // 430
								lineY_horizontalLineFifth = lineY_bookingCode + 115; // 545
								cmdCondition = '^BY3,3,80'+
											'^FO'+ lineX_bookingCode +','+ lineY_bookingCode +'^BCN,80,Y,N,N^FH^FD'+ bookingCode +'^FS'+
											'^FX Horizontal Line 5'+
											'^FO30,'+ lineY_horizontalLineFifth +'^GB750,3,3^FS';

								const resAllHistory = await AsyncStorage.getItem('@listAllHistory');
								const listAllHistories = JSON.parse(resAllHistory);

								const dtp_orderPackingItem = dtPackingListFinish.length > 0 ? dtPackingListFinish.filter(dt => {
									return dt.orderInfoId == dtOrderInfoId && dt.orderPackingInfoId == idData;
								}) : listAllHistories.filter(dt => {
									return dt.orderInfoId == dtOrderInfoId && dt.orderPackingInfoId == idData;
								})
								
								for (var arr = 0; arr < dtp_orderPackingItem.length; arr++) {
									if (dtOrderInfoId == dtp_orderPackingItem[arr].orderInfoId && idData == dtp_orderPackingItem[arr].orderPackingInfoId) {
										const lineX_dtpOrdPackItem_quantity = dtp_orderPackingItem[arr].quantity.toString().length == 1 ? 50 : 30;
										const index = dtp_orderPackingItem.indexOf(dtp_orderPackingItem[arr]);
										const lstIndex = dtp_orderPackingItem.lastIndexOf(dtp_orderPackingItem[arr]);

										if (dtp_orderPackingItem.length == 1) {
											if (index == coordinat.indexOf(coordinat[arr])) {
												cmdCondition += '^FO'+ lineX_dtpOrdPackItem_quantity +','+ coordinat[arr].first +'^A0N,45,45^FH^FD'+ dtp_orderPackingItem[arr].quantity +'^FS'+
															'^FO90,'+ coordinat[arr].first +'^A0N,20,20^FH^FD'+ dtp_orderPackingItem[arr].categoryDescription.replace(/\s+/g, '_C2_A0') +'^FS'+
															'^FO90,'+ coordinat[arr].second +'^A0N,20,20^FH^FD'+ dtp_orderPackingItem[arr].productDescription.replace(/\s+/g, '_C2_A0') +'^FS';
											}
										} else {
											if (index == coordinat.indexOf(coordinat[arr])) {
												cmdCondition += '^FO'+ lineX_dtpOrdPackItem_quantity +','+ coordinat[arr].first +'^A0N,45,45^FH^FD'+ dtp_orderPackingItem[arr].quantity +'^FS'+
															'^FO90,'+ coordinat[arr].first +'^A0N,20,20^FH^FD'+ dtp_orderPackingItem[arr].categoryDescription.replace(/\s+/g, '_C2_A0') +'^FS'+
															'^FO90,'+ coordinat[arr].second +'^A0N,20,20^FH^FD'+ dtp_orderPackingItem[arr].productDescription.replace(/\s+/g, '_C2_A0') +'^FS'+
															'^FO30,'+ coordinat[arr].third +'^GB750,3,3^FS';
											}
										}

										if (dtp_orderPackingItem.length == 13) {
											break;
										}
									}
								}

								if (dtp_orderPackingItem.length > 13) {
									var ct_quantity = 0;
									for (var lst = 13; lst < dtp_orderPackingItem.length; lst++) {
										if (dtOrderInfoId == dtp_orderPackingItem[lst].orderInfoId && idData == dtp_orderPackingItem[lst].orderPackingInfoId) {
											ct_quantity += dtp_orderPackingItem[lst].quantity;
										}
									}
									cmdCondition += '^FO30,'+ ((coordinat[12].third - minus) + 8) +'^A0N,20,20^FH^FDAnd_C2_A0more_C2_A0'+ ct_quantity +'_C2_A0pieces_C2_A0left...^FS';
								}
							} else { // this for without booking code
								const minus = 125;

								const resAllHistory = await AsyncStorage.getItem('@listAllHistory');
								const listAllHistories = JSON.parse(resAllHistory);

								const dtp_orderPackingItem = dtPackingListFinish.length > 0 ? dtPackingListFinish.filter(dt => {
									return dt.orderInfoId == dtOrderInfoId && dt.orderPackingInfoId == idData;
								}) : listAllHistories.filter(dt => {
									return dt.orderInfoId == dtOrderInfoId && dt.orderPackingInfoId == idData;
								})
								
								for (var arr = 0; arr < dtp_orderPackingItem.length; arr++) {
									if (dtOrderInfoId == dtp_orderPackingItem[arr].orderInfoId && idData == dtp_orderPackingItem[arr].orderPackingInfoId) {
										const lineX_dtpOrdPackItem_quantity = dtp_orderPackingItem[arr].quantity.toString().length == 1 ? 50 : 30;
										const index = dtp_orderPackingItem.indexOf(dtp_orderPackingItem[arr]);
										const lstIndex = dtp_orderPackingItem.lastIndexOf(dtp_orderPackingItem[arr]);
										
										if (dtp_orderPackingItem.length == 1) {
											if (index == coordinat.indexOf(coordinat[arr])) {
												cmdCondition += '^FO'+ lineX_dtpOrdPackItem_quantity +','+ (coordinat[arr].first - minus) +'^A0N,45,45^FH^FD'+ dtp_orderPackingItem[arr].quantity +'^FS'+
															'^FO90,'+ (coordinat[arr].first - minus) +'^A0N,20,20^FH^FD'+ dtp_orderPackingItem[arr].categoryDescription.replace(/\s+/g, '_C2_A0') +'^FS'+
															'^FO90,'+ (coordinat[arr].second - minus) +'^A0N,20,20^FH^FD'+ dtp_orderPackingItem[arr].productDescription.replace(/\s+/g, '_C2_A0') +'^FS';
											}
										} else {
											if (index == coordinat.indexOf(coordinat[arr])) {
												cmdCondition += '^FO'+ lineX_dtpOrdPackItem_quantity +','+ (coordinat[arr].first - minus) +'^A0N,45,45^FH^FD'+ dtp_orderPackingItem[arr].quantity +'^FS'+
															'^FO90,'+ (coordinat[arr].first - minus) +'^A0N,20,20^FH^FD'+ dtp_orderPackingItem[arr].categoryDescription.replace(/\s+/g, '_C2_A0') +'^FS'+
															'^FO90,'+ (coordinat[arr].second - minus) +'^A0N,20,20^FH^FD'+ dtp_orderPackingItem[arr].productDescription.replace(/\s+/g, '_C2_A0') +'^FS'+
															'^FO30,'+ (coordinat[arr].third - minus) +'^GB750,3,3^FS';
											}
										}

										if (dtp_orderPackingItem.length == 15) {
											break;
										}
									}
								}

								if (dtp_orderPackingItem.length > 15) {
									var ct_quantity = 0;
									for (var lst = 15; lst < dtp_orderPackingItem.length; lst++) {
										if (dtOrderInfoId == dtp_orderPackingItem[lst].orderInfoId && idData == dtp_orderPackingItem[lst].orderPackingInfoId) {
											ct_quantity += parseInt(dtp_orderPackingItem[lst].quantity);
										}
									}
									cmdCondition += '^FO30,'+ ((coordinat[14].third - minus) + 8) +'^A0N,20,20^FH^FDAnd_C2_A0more_C2_A0'+ ct_quantity +'_C2_A0pieces_C2_A0left...^FS';
								}
							}

							var convertDate = dtListOrder[lo].addTime;
							var tanggal = convertDate.substring(0, 10);
							var dmy = tanggal.split('-');
							var headerDate = dmy[2]+'/'+dmy[1]+'/'+dmy[0];

							if (cmdCondition !== undefined) {
								var dtCmdCondition = cmdCondition;
							} else {
								var dtCmdCondition = '';
							}

							var cmd = '^XA^PW812^CI28'+
									'^FX Top section with logo, name and address.'+
									'^BY3,3,80'+
									'^FO'+ X +','+ startAxisY +'^BCN,80,N,N,N^FD'+ dtOrderPackingInfoId +'^FS'+
									'^FO550,'+ lineY_verticalLineFirst +'^GB3,'+ lengthVerticalLineFirst +',2^FS'+
									'^FO570,'+ lineY_titleOrderId +'^A0N,22,22^FH^FDORDER_C2_A0ID^FS'+

									'^FO570,'+ lineY_orderId +'^A0N,22,22^FH^FD'+ dtListOrder[lo].orderInfoId +'^FS'+
									'^FO570,'+ lineY_date +'^A0N,22,22^FH^FD'+ headerDate +'^FS'+

									'^FX Horizontal Line 1'+
									'^FO30,'+ lineY_horizontalLineFirst +'^GB750,3,3^FS'+

									'^FO550,'+ lineY_verticalLineSecond +'^GB3,'+ lengthVerticalLineSecond +',2^FS'+
									'^FO30,'+ lineY_titleCourier +'^A0N,22,22^FH^FDCOURIER^FS'+
									'^FO570,'+ lineY_titlePackingId +'^A0N,22,22^FH^FDPACKING_C2_A0ID^FS'+

									'^FX Value for Courier'+
									'^FO30,'+ lineY_CourierName +'^A0N,23,23^FH^FD'+ shippingName.replace(/\s+/g, '_C2_A0') +'^FS'+
									'^FX Value for Packing Id'+
									'^FO570,'+ lineY_packingId +'^A0N,23,23^FH^FD'+ dtOrderPackingInfoId +'^FS'+

									'^FX Horizontal Line 2'+
									'^FO30,'+ lineY_horizontalLineSecond +'^GB750,3,3^FS'+

									'^FO30,'+ lineY_titleSender +'^A0N,22,22^FH^FDPENGIRIM^FS'+
									'^FO280,'+ lineY_verticalLineThird +'^GB3,'+ lengthVerticalLineThird +',2^FS'+
									'^FO300,'+ lineY_titleReceiver +'^A0N,22,22^FB480,30,0,L^FH^FDPENERIMA_C2_A0:_C2_A0'+ receiverName.replace(/\s+/g, '_C2_A0') +'^FS'+

									'^FX Value for Pengirim'+
									'^FO30,'+ lineY_senderName +'^A0N,23,23^FB250,30,0,L^FH^FD'+ orderInfoSenderName.replace(/\s+/g, '_C2_A0') +'^FS'+

									'^CF0,25'+
									'^FX Number Phone for Pengirim'+
									'^FO30,'+ lineY_senderContact +'^FH^FD'+ orderInfoSenderContact.replace(/\s+/g, '_C2_A0') +'^FS'+
									'^FX Number Phone for Penerima'+
									'^FO300,'+ lineY_receiverContact +'^FH^FD'+ receiverContact.replace(/\s+/g, '_C2_A0') +'^FS'+

									'^FX Horizontal Line 3'+
									'^FO30,'+ lineY_horizontalLineThird +'^GB750,3,3^FS'+

									'^FX Alamat'+
									'^FO30,'+ lineY_receiverAddress +'^A0N,22,22^FB750,30,-1,L,0^FH^CI28^FD'+ receiverAddress.replace(/\s+/g, '_C2_A0') +'^FS'+

									'^FX Horizontal Line 4'+
									'^FO30,'+ lineY_horizontalLineForth +'^GB750,3,3^FS'+dtCmdCondition+
									'^XZ';

							const netState = await NetInfo.fetch();
							if (netState.isConnected) {
								fetch('http://'+ipThermalPrinter+'/service/printercommand.lua?command='+encodeURIComponent(cmd)+'&pageid=Services&pagename=Send+Printer+Commands', {
									signal: abortController.signal,
								})
								.then(res => {
									setVisible(false);
									setLoadingPrintAlamat(false);
									setAbortConst(new AbortController());
								})
								.catch((error) => {
									setLoadingPrintAlamat(false);
									showErrorPrinter(error);
								});
							} else {
								setLoadingPrintAlamat(false);
								Alert.alert("Sorry","You're offline right now, please try again later");
							}
						}
					}
				} else {
					setLoadingPrintAlamat(false);
					Alert.alert("Sorry..!", "Something went wrong with list order.");
				}
			} else {
				setLoadingPrintAlamat(false);
				Alert.alert("Sorry..!", "can't found order packing ID");
			}
		}
	}

	const activateScan = () => {
		setScans(true);
		setScan(true);
	}

	const deactivateScan = () => {
		setScan(false);
	}

	const FlashOn = () => {
		setFlashButton(true);
		setTorchLight(RNCamera.Constants.FlashMode.torch);
	}

	const FlashOff = () => {
		setFlashButton(false);
		setTorchLight(RNCamera.Constants.FlashMode.off);
	}

	const getOrderInfoId = (barcodeCode) => {
		var stat = null;
		var x = null;
		var y = null;
		var z = null;
		for (var h = 0; h < dtListOrder.length; h++) {
			const packInfo = JSON.parse(dtListOrder[h].packingInfo);
			for (var i = 0; i < packInfo.length; i++) {
				if (barcodeCode == packInfo[i].orderPackingInfoId) {
					x = dtListOrder[h].orderInfoId;
					y = packInfo[i].total;
					z = packInfo[i].totalOrderPackingList;
					stat = 200;
					break;
				} else {
					stat = 404;
				}
			}
			if (stat == 200) {
				break;
			}
		}

		return {
			orderInfoId: x,
			totalPackInfo: y,
			totalOrderPackingList: z
		};
	}

	const cameraValidateSchema = Yup.object().shape({
		productId: Yup.string().required(),
		quantity: Yup.string().required(),
		orderInfoId: Yup.string().required(),
	});

	let globalData = {
		productId: null,
		orderPackInfo: null,
		orderInfoId: null,
		quantity: null,
	}

	const scanningReadBarcodeData = (scanResult) => {
		if (scanResult.data != null) {
			const barcodeCode = scanResult.data;

			if (barcodeCode.length == longBarcodeResult) {
				const sliceData = barcodeCode.slice(0, 12);
				const dataCode = ltrim(sliceData, '0');

				if (dataCode == lstbrg_firstArray.mainProductId && scans) {
					globalData.productId = lstbrg_firstArray.productId;
					setPrevProductId(dataCode);

					const productId = globalData.productId;

					if (dtListOrder.length == 1) {
						const single_orderInfoId = dtListOrder[0].orderInfoId;
						const arrsingle_packingInfo = JSON.parse(dtListOrder[0].packingInfo);
						const resArrSingle_packInfo = arrsingle_packingInfo.filter(d => {
							return d.total > d.totalOrderPackingList && d.statusOrderPackingInfo == 'Preparing';
						});

						if (resArrSingle_packInfo.length == 0) {
							setScans(false);
							setPrevProductId(null);
							generatePacking({ action: 'refresh' });
							const message = "Failed Scan, Please scan again...!";
							showAlertError(message);
						} else {
							if (productId !== null && single_orderInfoId == lstbrg_firstArray.orderInfoId && scans) {
								globalData.orderPackInfo = resArrSingle_packInfo[0].orderPackingInfoId;
								globalData.quantity = lstbrg_firstArray.quantity;
								globalData.orderInfoId = lstbrg_firstArray.orderInfoId;

								if (productId !== null && globalData.orderInfoId !== null && scans) {
									setScans(false);
									doSubmitData();
									RNBeep.PlaySysSound(RNBeep.AndroidSoundIDs.TONE_CDMA_ANSWER);
								}
							}
						}
					}
				} else if (prevProductId !== null && dataCode != lstbrg_firstArray.mainProductId && scans) {
					const data = getOrderInfoId(dataCode);
					if (data.orderInfoId == lstbrg_firstArray.orderInfoId) {
						globalData.productId = prevProductId;
						globalData.orderPackInfo = dataCode;
						globalData.quantity = lstbrg_firstArray.quantity;
						globalData.orderInfoId = lstbrg_firstArray.orderInfoId;

						if (globalData.orderInfoId !== null && scans) {
							setScans(false);
							doSubmitData();
							RNBeep.PlaySysSound(RNBeep.AndroidSoundIDs.TONE_CDMA_ANSWER);
						}
					} else if (data.orderInfoId != lstbrg_firstArray.orderInfoId) {
						setScans(false);
						const message = "Barcode yang anda scan tidak sesuai dengan PK-ID di Order List yang aktif..!";
						showAlertError(message);
					}
				} else if (dataCode != lstbrg_firstArray.mainProductId && (lstbrg_firstArray.mainProductId == '0' || lstbrg_firstArray.mainProductId == null) && scans) {
					const colorMessage = '#F89E00';
					const message = "Barcode scan tidak sesuai dengan Main Product ID dilist barang pertama, Silahkan sinkron data ke admin lalu refresh..!";
					showAlertError(message, colorMessage);
				} else {
					const colorMessage = '#EA0606';
					const message = "Barcode yang anda scan tidak sesuai dengan Product ID dilist barang yang pertama..!";
					showAlertError(message, colorMessage);
				}
			} else { // disini untuk proses barcode normal
				if (barcodeCode == lstbrg_firstArray.productId && scans) {
					globalData.productId = barcodeCode;
					setPrevProductId(barcodeCode);

					const productId = globalData.productId;

					if (dtListOrder.length == 1) {
						const single_orderInfoId = dtListOrder[0].orderInfoId;
						const arrsingle_packingInfo = JSON.parse(dtListOrder[0].packingInfo);
						const resArrSingle_packInfo = arrsingle_packingInfo.filter(d => {
							return d.total > d.totalOrderPackingList && d.statusOrderPackingInfo == 'Preparing';
						});

						if (resArrSingle_packInfo.length == 0) {
							setScans(false);
							setPrevProductId(null);
							generatePacking({ action: 'refresh' });
							const message = "Failed Scan, Please scan again...!";
							showAlertError(message);
						} else {
							if (productId !== null && single_orderInfoId == lstbrg_firstArray.orderInfoId && scans) {
								globalData.orderPackInfo = resArrSingle_packInfo[0].orderPackingInfoId;
								globalData.quantity = lstbrg_firstArray.quantity;
								globalData.orderInfoId = lstbrg_firstArray.orderInfoId;

								if (productId !== null && globalData.orderInfoId !== null && scans) {
									setScans(false);
									doSubmitData();
									RNBeep.PlaySysSound(RNBeep.AndroidSoundIDs.TONE_CDMA_ANSWER);
								}
							}
						}
					}
				} else if (prevProductId !== null && barcodeCode !== lstbrg_firstArray.productId && scans) {
					const data = getOrderInfoId(barcodeCode);
					if (data.orderInfoId == lstbrg_firstArray.orderInfoId) {
						globalData.productId = prevProductId;
						globalData.orderPackInfo = barcodeCode;
						globalData.quantity = lstbrg_firstArray.quantity;
						globalData.orderInfoId = lstbrg_firstArray.orderInfoId;

						if (globalData.orderInfoId !== null && scans) {
							setScans(false);
							doSubmitData();
							RNBeep.PlaySysSound(RNBeep.AndroidSoundIDs.TONE_CDMA_ANSWER);
						}
					} else if (data.orderInfoId != lstbrg_firstArray.orderInfoId) {
						setScans(false);
						const message = "Barcode yang anda scan tidak sesuai dengan PK-ID di Order List yang aktif..!";
						showAlertError(message);
					}
				} else {
					const colorMessage = '#EA0606';
					const message = "Barcode yang anda scan tidak sesuai dengan Product ID dilist barang yang pertama..!";
					showAlertError(message, colorMessage);
				}
			}
		}
	}

	const doSubmitData = async () => {
		setLoadingScanNew(true);
		const roles = await getRoles();
		const role_new_ordpkg = 'orderpacking_new';
		const role_view_ordpkg = 'orderpacking_view';
		const orderpacking_new = roles.includes(role_new_ordpkg);
		const orderpacking_view = roles.includes(role_view_ordpkg);
		const payload = {
			id: globalData.orderPackInfo,
			orderInfoId: globalData.orderInfoId,
			quantity: globalData.quantity,
			productId: globalData.productId
		}

		if (orderpacking_new) {
			dispatch(scanOrderPackingItemNew(payload)).then(async (res) => {
				console.log('res', res)
				if (res.success !== undefined) {
					for(var k = 0; k < dtListOrder.length; k++){
						if (dtListOrder[k].orderInfoId == payload.orderInfoId) {
							const packInfo = JSON.parse(dtListOrder[k].packingInfo);
							for(var m = 0; m < packInfo.length; m++){
								if (packInfo[m].orderPackingInfoId == payload.id) {
									const resultVal = parseInt(packInfo[m].totalOrderPackingList) + parseInt(payload.quantity);

									packInfo[m] = {...packInfo[m], totalOrderPackingList: resultVal}
								}
							}

							const strData = JSON.stringify(packInfo);
							dtListOrder[k] = {...dtListOrder[k], packingInfo: strData}
						}
					}
					setDtListOrder(dtListOrder);
					await AsyncStorage.setItem('@listOrder', JSON.stringify(dtListOrder));
					listBarang.splice(0, 1);
					setDtListBarang(listBarang);
					await AsyncStorage.setItem('@listBarang', JSON.stringify(listBarang));
					Object.assign(lstbrg_firstArray, {
						orderPackingInfoId: payload.id,
						orderPackingItemId: res.orderPackingItemId
					});
					setDtHistory([...dtHistory, lstbrg_firstArray]);
					await AsyncStorage.setItem('@listHistory', JSON.stringify([...dtHistory, lstbrg_firstArray]));

					const resultAllHistory = await AsyncStorage.getItem('@listAllHistory');
					const listAllHistory = JSON.parse(resultAllHistory);
					await AsyncStorage.setItem('@listAllHistory', JSON.stringify([...listAllHistory, lstbrg_firstArray]));

					if (res.success == "FINISH") {
						await AsyncStorage.setItem('@listHistory', JSON.stringify([]));
						asyncDataStorage();
						setScan(false);
						Alert.alert("Yeaayy..!", "This order packaging info has been finished...!");
					} else {
						if (listBarang.length == 0) {
							await AsyncStorage.setItem('@listHistory', JSON.stringify([]));
							asyncDataStorage();
							setScan(false);
							const msg = "List barang telah berhasil diselesaikan..";
							showAlertError(msg);
						}
					}
				}
				else if (res.failed !== undefined) {
					const failed = res.failed;
					Alert.alert("Owh No..!", failed);
				}
				
				globalData.productId = null;
				globalData.orderPackInfo = null;
				globalData.orderInfoId = null;
				globalData.quantity = null;

				if (dtListOrder !== undefined && dtListOrder.length > 0) {
					setPrevProductId(null);
				}
				setLoadingScanNew(false);
			}).catch(error => {
				setPrevProductId(null);
				setLoadingScanNew(false);
			});
		} else {
			setPrevProductId(null);
			setLoadingScanNew(false);
			const colorMessage = '#F89E00';
			const message = "You doesn't have access to send new data...!";
			showAlertError(message, colorMessage);
		}
	}

	const generatePacking = async (param) => {
		const roles = await getRoles();
		const role_view_ordpkg = 'orderpacking_view';
		const orderpacking_view = roles.includes(role_view_ordpkg);

		param.action == 'generate' && setLoadingGenerate(true);
		param.action == 'refresh' && setLoadingRefresh(true);
		const dt = await getDataLocation();
		const payload = {
			locationId: dt.locationId,
		}

		if (orderpacking_view) {
			await AsyncStorage.setItem('@listAllHistory', JSON.stringify([]));
			dispatch(getDataOrderPackingInfo(payload))
			.then(async (res) => {
				setPrevProductId(null);
				if (res.response !== undefined && res.response.failed !== undefined) {
					await AsyncStorage.setItem('@listOrder', JSON.stringify([]));
					await AsyncStorage.setItem('@listBarang', JSON.stringify([]));
					await AsyncStorage.setItem('@listHistory', JSON.stringify([]));
					asyncDataStorage();
					showAlertError(res.response.failed);
				} else {
					if (Array.isArray(res.orderInfoId) && (Array.isArray(res.packingList) || Array.isArray(res.packingListItem))) {
						if (param.action == 'generate' || param.action == 'refresh') {
							await AsyncStorage.setItem('@listOrder', JSON.stringify(res.orderInfoId));
						}
						await AsyncStorage.setItem('@listBarang', JSON.stringify(res.packingList == null ? [] : res.packingList));
						await AsyncStorage.setItem('@listHistory', JSON.stringify(res.packingListItem.reverse()));
						await AsyncStorage.setItem('@sessionId', JSON.stringify(res.sessionId));

						const latest = await AsyncStorage.getItem('@listAllHistory');
						const latestListHistory = JSON.parse(latest);
						if (latestListHistory.length != res.packingListItem.length) {
							await AsyncStorage.setItem('@listAllHistory', JSON.stringify(res.packingListItem.reverse()));
						}

						asyncDataStorage();
						param.action == 'generate' && setVisibleAlert(true);
					}
				}
				param.action == 'generate' && setLoadingGenerate(false);
				param.action == 'refresh' && setLoadingRefresh(false);
				param.action == 'refresh' && showAlertError('Refresh data success..!');
			})
			.catch(error => {
				param.action == 'generate' && setLoadingGenerate(false);
				param.action == 'refresh' && setLoadingRefresh(false);
				Alert.alert("Sorry, something went wrong.", error.message);
			});
		} else {
			const colorMessage = '#F89E00';
			const message = "You doesn't have access to generate data...!";
			showAlertError(message, colorMessage);
			param.action == 'generate' && setLoadingGenerate(false);
			param.action == 'refresh' && setLoadingRefresh(false);
		}
	}

	const printAllDataOrderPack = async () => {
		setLoadingPrintAll(true);
		const dt = await getDataLocation();
		var ipThermalPrinter = dt.ipThermalPrinter;

		if (ipThermalPrinter == null || ipThermalPrinter == '') {
			setLoadingPrintAll(false);
			hideDialogAlert();
			const colorMessage = '#F89E00';
			const message = "Silahkan tetapkan nilai IP Printer terlebih dahulu...!";
			showAlertError(message, colorMessage);
		} else {
			if (Array.isArray(dtListOrder) && dtListOrder.length > 0) {
				var arrData = new Array();
				dtListOrder.map(orderinfo => {
					const packInfo = JSON.parse(orderinfo.packingInfo);
					packInfo.map(j => {
						arrData.push(j.orderPackingInfoId);
					});
				});

				var cmd = '';
				for (var i = 0; i < dtListOrder.length; i++) {
					const idData = arrData[i].toString();
					const X = getAxisCenterHeaderBarcode(idData);

					const receiverName = dtListOrder[i].receiverName;
					const orderInfoReceiverAddress = dtListOrder[i].receiverAddress.replace(/\s\s+/g, ' ');
					const kotaName = dtListOrder[i].kotaName ? ', '+dtListOrder[i].kotaName : null;
					const kelurahanName = dtListOrder[i].kelurahanName ? ', '+dtListOrder[i].kelurahanName : null;
					const provinsiName = dtListOrder[i].provinsiName ? ', '+dtListOrder[i].provinsiName : null;
					const postalCode = dtListOrder[i].postalCode ? ', '+dtListOrder[i].postalCode : null;
					const receiverContact = dtListOrder[i].receiverContact;
					const receiverAddress = orderInfoReceiverAddress+kelurahanName+kotaName+provinsiName+postalCode;
					const shippingName = dtListOrder[i].shippingName;
					const orderInfoSenderName = dtListOrder[i].senderName;
					const orderInfoSenderContact = dtListOrder[i].senderContact;

					const lengthReceiverName = receiverName.length;
					const lengthOrderInfoSenderName = orderInfoSenderName.length;
					const lengthReceiverContact = receiverContact.length;
					const totalLengthReceiverName = parseInt(lengthReceiverName);
					const totalLengthOrderInfoSenderName = parseInt(lengthOrderInfoSenderName);

					/**
					 * zpl code for whitespace in hex UTF-8 is C2 A0
					 * use ^FH with indicator _ (underscore is default) to ^FD
					 * example : ^FO50,100^A0N,51,51^FH^FDexample_C2_A0text
					*/

					var cmdCondition, bookingCode, lineX_bookingCode, lineY_bookingCode;
					var lineY_horizontalLineFirst, lineY_horizontalLineSecond, lineY_horizontalLineThird, lineY_horizontalLineForth, lineY_horizontalLineFifth, extraAxisY1, extraAxisY2;
					var lengthVerticalLineFirst, lengthVerticalLineSecond, lengthVerticalLineThird;
					var startAxisY, lineY_titleOrderId, lineY_verticalLineFirst, lineY_orderId, lineY_date, lineY_verticalLineSecond, lineY_titleCourier, lineY_titlePackingId, lineY_CourierName, lineY_packingId, lineY_verticalLineThird, lineY_titleSender, lineY_titleReceiver, lineY_senderName, lineY_senderContact, lineY_receiverContact, lineY_receiverAddress;
					var lineY_dtpOrdPackItem_quantity, lineY_dtpOrdPackItem_categoryName, lineY_dtpOrdPackItem_productDescription;
					var lineY_next_dtpOrdPackItem_quantity, lineY_next_dtpOrdPackItem_categoryName, lineY_next_dtpOrdPackItem_productDescription;

					if (totalLengthOrderInfoSenderName > 42 && totalLengthOrderInfoSenderName <= 95) {
						extraAxisY1 = 25;
					} else if (totalLengthOrderInfoSenderName > 95) {
						extraAxisY1 = 50;
					} else {
						extraAxisY1 = 0;
					}

					if (totalLengthReceiverName > 42 && totalLengthReceiverName <= 95) {
						extraAxisY2 = 20;
					} else if (totalLengthReceiverName > 95) {
						extraAxisY2 = 50;
					} else {
						extraAxisY2 = 0;
					}

					startAxisY = 10;
					lineY_titleOrderId = startAxisY + 10; // 20
					lineY_verticalLineFirst = startAxisY;
					lineY_orderId = lineY_titleOrderId + 25; // 45
					lineY_date = lineY_orderId + 25; // 70

					lengthVerticalLineFirst = 85;
					lengthVerticalLineSecond = 65;
					lengthVerticalLineThird = 105;

					lineY_horizontalLineFirst = startAxisY + lengthVerticalLineFirst + 5; // 100

					lineY_verticalLineSecond = lineY_horizontalLineFirst + 10; // 110
					lineY_titleCourier = lineY_horizontalLineFirst + 15; // 115
					lineY_titlePackingId = lineY_horizontalLineFirst + 15; // 115
					lineY_CourierName = lineY_titleCourier + 30; // 145
					lineY_packingId = lineY_titlePackingId + 30; // 145

					lineY_horizontalLineSecond = lineY_verticalLineSecond + lengthVerticalLineSecond + 5; // 180

					lineY_verticalLineThird = lineY_horizontalLineSecond + 10; // 190
					lineY_titleSender = lineY_horizontalLineSecond + 15; // 195
					lineY_titleReceiver = lineY_horizontalLineSecond + 15; // 195
					lineY_senderName = lineY_titleSender + 25; // 220
					lineY_senderContact = lineY_senderName + 25 + extraAxisY1; // 245
					lineY_receiverContact = lineY_titleReceiver + 25 + extraAxisY2; // 220 / 240 / 270

					lineY_horizontalLineThird = lineY_verticalLineThird + lengthVerticalLineThird + 5; // 300

					lineY_receiverAddress = lineY_horizontalLineThird + 10; // 310

					lineY_horizontalLineForth = lineY_receiverAddress + 110; // 420

					var convertDate = dtListOrder[i].addTime;
					var tanggal = convertDate.substring(0, 10);
					var dmy = tanggal.split('-');
					var headerDate = dmy[2]+'/'+dmy[1]+'/'+dmy[0];

					cmd += '^XA^PW812^CI28'+

						'^FX Top section with logo, name and address.'+
						'^BY3,3,80'+
						'^FO'+ X +','+ startAxisY +'^BCN,80,N,N,N^FD'+ idData +'^FS'+
						'^FO550,'+ lineY_verticalLineFirst +'^GB3,'+ lengthVerticalLineFirst +',2^FS'+
						'^FO570,'+ lineY_titleOrderId +'^A0N,22,22^FH^FDORDER_C2_A0ID^FS'+

						'^FO570,'+ lineY_orderId +'^A0N,22,22^FH^FD'+ dtListOrder[i].orderInfoId +'^FS'+
						'^FO570,'+ lineY_date +'^A0N,22,22^FH^FD'+ headerDate +'^FS'+

						'^FX Horizontal Line 1'+
						'^FO30,'+ lineY_horizontalLineFirst +'^GB750,3,3^FS'+

						'^FO550,'+ lineY_verticalLineSecond +'^GB3,'+ lengthVerticalLineSecond +',2^FS'+
						'^FO30,'+ lineY_titleCourier +'^A0N,22,22^FH^FDCOURIER^FS'+
						'^FO570,'+ lineY_titlePackingId +'^A0N,22,22^FH^FDPACKING_C2_A0ID^FS'+

						'^FX Value for Courier'+
						'^FO30,'+ lineY_CourierName +'^A0N,23,23^FH^FD'+ shippingName.replace(/\s+/g, '_C2_A0') +'^FS'+
						'^FX Value for Packing Id'+
						'^FO570,'+ lineY_packingId +'^A0N,23,23^FH^FD'+ idData +'^FS'+

						'^FX Horizontal Line 2'+
						'^FO30,'+ lineY_horizontalLineSecond +'^GB750,3,3^FS'+

						'^FO30,'+ lineY_titleSender +'^A0N,22,22^FH^FDPENGIRIM^FS'+
						'^FO280,'+ lineY_verticalLineThird +'^GB3,'+ lengthVerticalLineThird +',2^FS'+
						'^FO300,'+ lineY_titleReceiver +'^A0N,22,22^FB480,30,0,L^FH^FDPENERIMA_C2_A0:_C2_A0'+ receiverName.replace(/\s+/g, '_C2_A0') +'^FS'+

						'^FX Value for Pengirim'+
						'^FO30,'+ lineY_senderName +'^A0N,23,23^FB480,30,0,L^FH^FD'+ orderInfoSenderName.replace(/\s+/g, '_C2_A0') +'^FS'+

						'^CF0,25'+
						'^FX Number Phone for Pengirim'+
						'^FO30,'+ lineY_senderContact +'^FH^FD'+ orderInfoSenderContact.replace(/\s+/g, '_C2_A0') +'^FS'+
						'^FX Number Phone for Penerima'+
						'^FO300,'+ lineY_receiverContact +'^FH^FD'+ receiverContact.replace(/\s+/g, '_C2_A0') +'^FS'+

						'^FX Horizontal Line 3'+
						'^FO30,'+ lineY_horizontalLineThird +'^GB750,3,3^FS'+

						'^FX Alamat'+
						'^FO30,'+ lineY_receiverAddress +'^A0N,22,22^FB750,30,-1,L,0^FH^CI28^FD'+ receiverAddress.replace(/\s+/g, '_C2_A0') +'^FS'+

						'^FX Horizontal Line 4'+
						'^FO30,'+ lineY_horizontalLineForth +'^GB750,3,3^FS'+
						'^XZ';
				}

				fetch('http://'+ipThermalPrinter+'/service/printercommand.lua?command='+encodeURIComponent(cmd)+'&pageid=Services&pagename=Send+Printer+Commands', {
					signal: abortController.signal,
				})
				.then(res => {
					setLoadingPrintAll(false);
					hideDialogAlert();
					setAbortConst(new AbortController());
				})
				.catch((error) => {
					setLoadingPrintAll(false);
					showErrorPrinter(error);
				});
			}
		}
	}

	const showAlertError = (pesanError, color = '#000') => {
		setDataAlertError(pesanError);
		setColorAlert(color);
		setAlertError(true);
	}

	const dismissAlertError = () => setAlertError(false);

	const showErrorPrinter = (error) => {
		setErrPrinter(error);
		setVisibleErrPrinter(true);
	}

	const dismissErrPrinter = () => setVisibleErrPrinter(false);

	const showInfoForPrint = async (orderPackingInfoId, orderInfoId) => {
		setDtOrderPackingInfoId(orderPackingInfoId);
		setDtOrderInfoId(orderInfoId);
		const dataFilter = _.filter(dtListOrder, ['orderInfoId', orderInfoId]);
		await setDtDetailPrint(dataFilter);
		setVisible(true);
	}

	const hideDialog = () => setVisible(false);
	const hideDialogAlert = () => setVisibleAlert(false);

	const handlerFinishButton = (params) => {
		Alert.alert("Are you sure..?", "apakah anda yakin untuk mem-finish order info ini..?", [
			{
				text: "Cancel"
			},
			{
				text: "Yes",
				onPress: async () => {
					const roles = await getRoles();
					const role_new_ordpkg = 'orderpacking_new';
					const orderpacking_new = roles.includes(role_new_ordpkg);

					if (orderpacking_new) {
						const dt = await getDataLocation();
						const payloadOrderPackFinish = { packingInfoId: params[0] }
						const response = await dispatch(orderPackingFinish(payloadOrderPackFinish));

						const dtsession = await AsyncStorage.getItem('@sessionId');
						const payloadSession = { sessionId: JSON.parse(dtsession), locationId: dt.locationId } // with new param
						const lstOrd = await dispatch(orderPackingViewAllOrderInfoId(payloadSession));

						const data = await AsyncStorage.getItem('@listOrder');
						const orderList = JSON.parse(data);
						if (orderList != null) {
							if (orderList.length == lstOrd.length) {
								await AsyncStorage.setItem('@listOrder', JSON.stringify(lstOrd));
							} else {
								await AsyncStorage.setItem('@listOrder', JSON.stringify(orderList));
							}
						} else {
							await AsyncStorage.setItem('@listOrder', JSON.stringify(lstOrd));
						}
						await AsyncStorage.setItem('@listHistory', JSON.stringify([]));
						asyncDataStorage();

						if (response.response.success !== undefined) {
							params.shift();
							const message = response.response.success;
							Alert.alert("Success..!", message);
						} else if (response.response.failed !== undefined) {
							const message = response.response.failed;
							Alert.alert("Failed..!", message);
						}
					} else {
						const colorMessage = '#F89E00';
						const message = "You doesn't have access to submit...!";
						showAlertError(message, colorMessage);
					}
				}
			}
		]);
	}

	const barcodeFormInput = () => {
		setScan(false);
		setVisibleBcForm(true);
	}

	const hiddenBcForm = () => setVisibleBcForm(false);

	const processDeleteHistory = async (param) => {
		const payload = {
			orderPackingItemId: param.orderPackingItemId
		}
		const role_new_ordpkg = 'orderpacking_new';
		const orderpacking_new = roles.includes(role_new_ordpkg);

		if (orderpacking_new) {
			const result = await dispatch(deleteDataHistoryPackingList(payload));
			if (result.response.success !== undefined) {
				const resListOrder = await AsyncStorage.getItem('@listOrder');
				const lstDtOrder = JSON.parse(resListOrder);
				for(var k = 0; k < lstDtOrder.length; k++){
					if (lstDtOrder[k].orderInfoId == param.orderInfoId) {
						const packInfo = JSON.parse(lstDtOrder[k].packingInfo);
						for(var m = 0; m < packInfo.length; m++){
							if (packInfo[m].orderPackingInfoId == param.orderPackingInfoId) {
								const resultVal = parseInt(packInfo[m].totalOrderPackingList) - parseInt(param.quantity);

								packInfo[m] = {...packInfo[m], totalOrderPackingList: resultVal}
							}
						}

						const strData = JSON.stringify(packInfo);
						lstDtOrder[k] = {...lstDtOrder[k], packingInfo: strData}
					}
				}
				await AsyncStorage.setItem('@listOrder', JSON.stringify(lstDtOrder));

				const resListHistory = await AsyncStorage.getItem('@listHistory');
				const lstDtHistory = JSON.parse(resListHistory);

				const delList = await _.remove(lstDtHistory, (n) => {
					return n.orderPackingItemId == param.orderPackingItemId;
				});
				const resultAfterInsertKey = await delList.map(item => {
					return Object.assign(item, {
						type: 'Pcs',
						mainProductId: param.productId
					});
				});

				const resListBarang = await AsyncStorage.getItem('@listBarang');
				const lstDtBarang = JSON.parse(resListBarang);

				const newLstDtBarang = [...lstDtBarang, resultAfterInsertKey[0]];
				const res = _.orderBy(newLstDtBarang, ['position','noUrut'], ['asc','asc']);

				await AsyncStorage.setItem('@listBarang', JSON.stringify(res));
				await AsyncStorage.setItem('@listHistory', JSON.stringify(lstDtHistory));
				asyncDataStorage();
			} else {
				const colorMessage = '#EA0606';
				showAlertError(result.response.failed, colorMessage);
			}
		} else {
			const colorMessage = '#EF9A15';
			const message = "You doesn't have access to delete data...!";
			showAlertError(result.response.failed, colorMessage);
		}
	}

    return (
		<View style={styles.container}>
			<Appbar.Header style={styles.appHeader}>
				<Appbar.BackAction onPress={() => {
					navigation.goBack();
					deactivateScan();
				}} />
				<Appbar.Content title="Packing List" subtitle={'Order'} />
				{
					loadingRefresh ? (
						<TouchableOpacity onPress={() => {
							if (loadingRefresh) {
								cancelFetch();
								setLoadingRefresh(false);
							}
						}}>
							<ActivityIndicator
								size={17}
								color="#fff"
								style={{
									marginRight: 16,
									padding: 0
								}}
							/>
						</TouchableOpacity>
					) : (
						<Appbar.Action
							icon="refresh"
							onPress={() => {
								generatePacking({ action: 'refresh' })
							}}
						/>
					)
				}
			</Appbar.Header>
			<SafeAreaView style={styles.container}>
				<Spinner
		          visible={loadingScanNew}
		          textStyle={{
		          	color: '#41ABF6'
		          }}
		          color="#41ABF6"
		          overlayColor="rgba(0, 0, 0, 0.45)"
		          animation="fade"
		          children={
		          	<View style={{
		          		flex: 1,
		          		justifyContent: 'center',
		          		alignItems: 'center'
		          	}}>
		          		<ActivityIndicator
		          			size="large"
							color="#fff"
		          		/>
		          		<Text style={{
		          			color: '#fff',
		          			fontSize: 20,
		          			margin: 10
		          		}}>{'Please Wait...'}</Text>
		          		<Button mode="contained" color="#fff" onPress={() => {
		          			cancelFetch();
		          			setPrevProductId(null);
		          			generatePacking({ action: 'refresh' });
		          			setLoadingScanNew(false);
		          		}}>
		          			{'Stop Proccess'}
		          		</Button>
		          	</View>
		          }
		        />
				{
					scan && navigation.isFocused() ? (
						<RNCamera
							ref={ref => {
								setCamera(ref);
							}}
							defaultTouchToFocus
							barCodeTypes={[RNCamera.Constants.BarCodeType.code128]}
							flashMode={torchLight}
							mirrorImage={false}
							onBarCodeRead={(param) => {
								scans && scanningReadBarcodeData(param);
							}}
							style={[styles.sizeBoxCameraScan, styles.styleBoxCameraScan]}
							ration={'1:1'}
							type={typeCamera}
							onTap={(x) => {
								setScans(true);
							}}
							>
							{scans && <BarcodeMask width={400} height={150} edgeWidth={0} edgeHeight={0} />}
							<View style={styles.buttonOnLeftCamera}>
								{ flashButton && scan ? (
					        		<IconButton icon="flash-off" size={24} color="#000" style={styles.buttonFloatOnCameraBox} onPress={FlashOff} />
					        	) : (
					        		<IconButton icon="flash-outline" size={24} color="#000" style={styles.buttonFloatOnCameraBox} onPress={FlashOn} />
					        	) }

					        	<IconButton icon="barcode-off" size={24} color="#000" style={styles.buttonFloatOnCameraBox} onPress={deactivateScan} />
							</View>
							<View style={styles.buttonOnRightCamera}>
								<IconButton icon="pencil-outline" size={24} color="#000" style={styles.buttonFloatOnCameraBox} onPress={barcodeFormInput} />
							</View>
						</RNCamera>
					) : (
						<View style={[styles.sizeBoxCameraScan, styles.styleBoxCameraScan]}>
							{ Array.isArray(listBarang) && listBarang.length > 0 && Array.isArray(dtListOrder) ? (
									<Button icon="barcode" mode="outline" color="#000" style={styles.buttonOpenCameraScan} onPress={activateScan}>Open Scanner</Button>
								) : (
									<Button icon="play-outline" mode="outline" color="#000" loading={loadingGenerate} style={styles.buttonOpenCameraScan} onPress={() => {
										if (loadingGenerate) {
											cancelFetch();
											setLoadingGenerate(false);
										} else {
											generatePacking({ action: 'generate' });
										}
									}}>Start</Button>
								)
							}
	  					</View>
					)
				}

				{/*awal tampilan daftar order info*/}
				<View style={[styles.containerOrderPacking, styles.styleboxOrderInfo]}>
					<View>
						{
							Array.isArray(dtListOrder) && dtListOrder.length > 0 ? (
								<FlatList
									numColumns={3}
									data={dtListOrder}
									keyExtractor={(item, index) => index}
									renderItem={({ item, index }) => {
										const packingInfo = JSON.parse(item.packingInfo);
										const dtPackingInfo = _.orderBy(packingInfo, ['orderPackingInfoId'], ['desc']);
										var border = false;
										if (lstbrg_firstArray) {
											if (lstbrg_firstArray.orderInfoId == item.orderInfoId) {
					        					border = true;
					        				}
										}

										const dtArr = _.compact(dtPackingInfo.map(dt => {
											if (item.doublePackage != 'Yes' && dt.totalOrderPackingList != dt.total && dt.totalOrderPackingList < dt.total && dt.statusOrderPackingInfo == 'Preparing') {
												return dt.orderPackingInfoId;
											}
										}));

										return (
											<View style={{
												flex: 1,
												paddingBottom: 5,
												justifyContent: 'space-between',
												alignItems: 'center'
												}}>
												<View style={styles.listOrderPackStyle}>
													<ScrollView
														contentContainerStyle={styles.contentContainerStyle}
								        				style={{
															borderWidth: 1,
															borderColor: border ? '#1EB206' : '#000'
								        				}}>
														{
															dtPackingInfo?.filter(ft => { return ft.statusOrderPackingInfo == 'Preparing' }).map((packInfo, keyIndex) => {
																return (
																	<TouchableOpacity key={keyIndex} onPress={() => showInfoForPrint(packInfo.orderPackingInfoId, item.orderInfoId)}>
																		<Text style={[styles.textInScrollView, { color: packInfo.statusOrderPackingInfo == 'Preparing' ? '#1EB206' : '#000' }]}>
																			<Subheading style={ styles.textSubHeading }>
																				{'PK-ID : '+packInfo.orderPackingInfoId+'\n'}
																				{/*  'PK-ID : 222023'+'\n' */}
																			</Subheading>
																			{packInfo.totalOrderPackingList + ' / ' + packInfo.total}
																			{/*  '1232/2320'  */}
																		</Text>
																	</TouchableOpacity>
																)
															})
														}
													</ScrollView>
												</View>
												<Text>{'O-ID : '+item.orderInfoId}</Text>
												<SafeAreaView style={{ flexDirection: 'row', paddingTop: 5, marginHorizontal: 1 }}>
									        		{
								        				dtArr.length > 0 && (
								        					<Button
								        						mode="outlined"
								        						color="#110F0F"
								        						onPress={ () => handlerFinishButton(dtArr) }
								        						labelStyle={{
								        							fontSize: 8,
								        							marginHorizontal: 10,
								        							marginVertical: 5,
								        							fontWeight: 'bold',
								        							width: wp('25%')
								        						}}
								        					>
								        						{'Submit'}
								        						{/*  'O-ID : 220211'+' | Submit'  */}
								        					</Button>
								        				)
									        		}
								        		</SafeAreaView>
											</View>
										)
									}}
								/>
							) : (
								<View style={{ alignItems: 'center', justifyContent: 'center' }}>
					        		<Text style={{ fontWeight: '100', color: 'lightgray', fontSize: 20, fontStyle: 'italic' }}>Data is empty</Text>
					        	</View>
							)
						}
					</View>
				</View>
				{/*akhir tampilan daftar order info*/}

				{/*awal tampilan list barang dan history*/}
				<View style={[styles.containerFinishPacking, styles.styleBoxPackingList]}>
					<Card style={{ flex: 1, backgroundColor: 'white' }}>
	        			<Card.Title titleStyle={{ fontSize: 16, color: 'black', marginBottom: 0, marginVertical: 1 }} style={{ minHeight: 20 }} title="List Barang :"/>
	        			<Card.Content>
	        				<FlatList
							  	contentContainerStyle={{ paddingBottom: 30 }}
							  	data={listBarang}
							  	keyExtractor={(item, index) => index}
							  	initialNumToRender={5}
							  	renderItem={({ item, index }) => (
							  		<View style={{
							    		flex: 1,
							    		flexDirection: 'row',
							    		marginVertical: 1,
							    		backgroundColor: prevProductId !== null && index === 0 ? '#4cae4c' : '#fff'
							    	}}>
								    	<View style={{
								    		flexDirection: 'row',
								    		alignItems: 'flex-start',
								    		justifyContent: 'flex-end',
								    		padding: 3,
								    		width: wp('19%'),
								    		borderRightWidth: 1.5,
								    		borderRightColor: '#000'
								    	}}>
								    		<Text style={{ color: '#000', fontSize: 32, lineHeight: 40 }}>{ item.quantity }</Text>
							    			<Text style={{ color: '#000', fontSize: 12, lineHeight: 25 }}>{ item.type }</Text>
								    	</View>
								    	<View style={{
								    		padding: 3
								    	}}>
								    		<Text style={{ color: '#000', fontSize: 20, fontWeight: 'bold' }}>{ item.categoryDescription }</Text>
								    		<Text style={{ color: '#000', fontSize: 12 }}>{ item.productDescription && item.productDescription.replace('Size', '-') }</Text>
								    	</View>
								    	<View style={{
								    		flex: 1,
								    		padding: 3,
								    		alignItems: 'flex-end',
								    		justifyContent: 'center'
								    	}}>
								    		<Text style={{ color: '#000', fontSize: 12 }}>{ item.position }</Text>
								    		<Text style={{ color: '#000', fontSize: 12 }}>{ item.productId }</Text>
								    	</View>
							      </View>
							  	)}
							  	ItemSeparatorComponent={() => (
							  		<Line/>
							  	)}
							  	ListEmptyComponent={() => (
							  		<View style={{
							    		flex: 1,
							    		flexDirection: 'row',
							    		marginVertical: 1,
							    		backgroundColor: '#fff'
							    	}}>
					  					<Text>{ 'Empty Data' }</Text>
					  				</View>
							  	)}
							/>
	        			</Card.Content>
	        		</Card>
	        		<Card style={{ flex: 1, backgroundColor: 'white' }}>
	        			<Card.Title titleStyle={{ fontSize: 16, color: 'black', marginBottom: 0, marginVertical: 1 }} style={{ minHeight: 20 }} title="History :"/>
	        			<Card.Content>
	        				<SwipeProvider isFocusedScreen={navigation.isFocused()}>
		        				<FlatList
								  	contentContainerStyle={{ paddingBottom: 30 }}
								  	data={dtPackingListFinish}
								  	keyExtractor={(item, index) => index}
								  	renderItem={({ item, index }) => {
								  		return (
								  			<SwipeableRow
								  				itemKey={item.orderPackingItemId}
								  				rightButtons={[
								  					{
								  						text: 'Delete',
								  						iconName: 'trash-can-outline',
								  						color: '#fff',
								  						x: 64,
								  						onPress: () => processDeleteHistory(item)
								  					}
								  				]}
								  				>
									  			<View key={index} style={{
										    		flex: 1,
										    		flexDirection: 'row',
										    		marginVertical: 1,
										    		backgroundColor: '#fff',
										    	}}>
											    	<View style={{
											    		flexDirection: 'row',
											    		alignItems: 'flex-start',
											    		justifyContent: 'flex-end',
											    		padding: 3,
											    		width: wp('19%'),
											    		borderRightWidth: 1.5,
											    		borderRightColor: '#000'
											    	}}>
											    		<Text style={{ color: '#000', fontSize: 32, lineHeight: 40 }}>{ item.quantity }</Text>
										    			<Text style={{ color: '#000', fontSize: 12, lineHeight: 25 }}>{ 'Pcs' }</Text>
											    	</View>
											    	<View style={{
											    		padding: 3
											    	}}>
											    		<Text style={{ color: '#000', fontSize: 20, fontWeight: 'bold' }}>{ item.categoryDescription }</Text>
											    		<Text style={{ color: '#000', fontSize: 12 }}>{ item.productDescription && item.productDescription.replace('Size', '-') }</Text>
											    	</View>
											    	<View style={{
											    		flex: 1,
											    		padding: 3,
											    		alignItems: 'flex-end',
											    		justifyContent: 'center'
											    	}}>
											    		<Text style={{ color: '#000', fontSize: 12 }}>{ item.position }</Text>
											    		<Text style={{ color: '#000', fontSize: 12 }}>{ item.productId }</Text>
											    	</View>
										      	</View>
								  			</SwipeableRow>
									  	)
								  	}}
								  	ItemSeparatorComponent={() => (
								  		<Line/>
								  	)}
								  	ListEmptyComponent={() => (
								  		<View style={{
								    		flex: 1,
								    		flexDirection: 'row',
								    		marginVertical: 1,
								    		backgroundColor: '#fff'
								    	}}>
						  					<Text>{ 'Empty Data' }</Text>
						  				</View>
								  	)}
								/>
	        				</SwipeProvider>
	        			</Card.Content>
	        		</Card>
				</View>
				{/*akhir tampilan list barang dan history*/}

				<Portal>
					<Dialog visible={visible} onDismiss={hideDialog} style={{ backgroundColor: '#fff' }}>
						<View style={{
							display: 'flex',
							justifyContent: 'center',
							alignItems: 'flex-end'
						}}>
							<IconButton
								icon="close"
								color="#000"
								size={20}
								onPress={hideDialog}
							/>
						</View>
						<Dialog.ScrollArea>
							<ScrollView>
								<View>
									<DataTable>
										<DataTable.Row>
											<DataTable.Cell style={styles.flexKeyNameColumn}>{ 'Order ID' }</DataTable.Cell>
											<DataTable.Cell style={styles.flexColumnSeparate}>{ ':' }</DataTable.Cell>
											<DataTable.Cell>{ dtDetailPrint.length == 1 && dtDetailPrint[0].orderInfoId }</DataTable.Cell>
										</DataTable.Row>
										<DataTable.Row>
											<DataTable.Cell style={styles.flexKeyNameColumn}>{ 'Packing ID' }</DataTable.Cell>
											<DataTable.Cell style={styles.flexColumnSeparate}>{ ':' }</DataTable.Cell>
											<DataTable.Cell>{ dtOrderPackingInfoId }</DataTable.Cell>
										</DataTable.Row>
										<DataTable.Row>
											<DataTable.Cell style={styles.flexKeyNameColumn}>{ 'Shipping' }</DataTable.Cell>
											<DataTable.Cell style={styles.flexColumnSeparate}>{ ':' }</DataTable.Cell>
											<DataTable.Cell>{ dtDetailPrint.length == 1 && dtDetailPrint[0].shippingName }</DataTable.Cell>
										</DataTable.Row>
										<DataTable.Row>
											<DataTable.Cell style={styles.flexKeyNameColumn}>{ 'Sender' }</DataTable.Cell>
											<DataTable.Cell style={styles.flexColumnSeparate}>{ ':' }</DataTable.Cell>
											<DataTable.Cell>{ dtDetailPrint.length == 1 && dtDetailPrint[0].senderName }</DataTable.Cell>
										</DataTable.Row>
										<DataTable.Row>
											<DataTable.Cell style={styles.flexKeyNameColumn}>{ 'Receiver' }</DataTable.Cell>
											<DataTable.Cell style={styles.flexColumnSeparate}>{ ':' }</DataTable.Cell>
											<TouchableRipple style={{
												flex: 1,
											    flexDirection: 'row',
											    alignItems: 'center',
											    justifyContent: 'flex-start',
											}}>
												<Text numberOfLines={3}>{ dtDetailPrint.length == 1 && dtDetailPrint[0].receiverName }</Text>
											</TouchableRipple>
										</DataTable.Row>
										<DataTable.Row>
											<DataTable.Cell style={styles.flexKeyNameColumn}>{ 'Receiver Contact' }</DataTable.Cell>
											<DataTable.Cell style={styles.flexColumnSeparate}>{ ':' }</DataTable.Cell>
											<DataTable.Cell>{ dtDetailPrint.length == 1 && dtDetailPrint[0].receiverContact }</DataTable.Cell>
										</DataTable.Row>
										<DataTable.Row>
											<DataTable.Cell style={styles.flexKeyNameColumn}>{ 'Address' }</DataTable.Cell>
											<DataTable.Cell style={styles.flexColumnSeparate}>{ ':' }</DataTable.Cell>
											<TouchableRipple style={{
												flex: 1,
											    flexDirection: 'row',
											    alignItems: 'center',
											    justifyContent: 'flex-start',
											}}>
												<Text numberOfLines={10}>{ dtDetailPrint.length == 1 && [dtDetailPrint[0].receiverAddress, dtDetailPrint[0].kelurahanName, dtDetailPrint[0].kotaName, dtDetailPrint[0].provinsiName, dtDetailPrint[0].postalCode].join(', ') }</Text>
											</TouchableRipple>
										</DataTable.Row>
										<Line/>
									</DataTable>
								</View>
							</ScrollView>
						</Dialog.ScrollArea>
						<Dialog.Actions style={{
				        	justifyContent: 'space-between',
				        	marginLeft: 15,
				        	marginRight: 15,
				        }}>
							<Button
							icon="printer"
								mode="outlined"
								uppercase={false}
								loading={loadingPrintPackingList}
								onPress={printPackingList}
								labelStyle={{
									color: '#000'
								}}
							>
							{'Packing List'}
							</Button>
							<Button
							icon="printer"
								mode="outlined"
								uppercase={false}
								loading={loadingPrintAlamat}
								onPress={printAlamat}
								labelStyle={{
									color: '#000'
								}}
							>
							{'Alamat'}
							</Button>
						</Dialog.Actions>
					</Dialog>
				</Portal>

				<Portal>
			    	<Dialog visible={visibleAlert} onDismiss={hideDialogAlert} style={{ backgroundColor: '#fff' }}>
			    		<Dialog.Title style={{ color: '#000' }}>Label Print</Dialog.Title>
				        <Dialog.Content>
				        	<Paragraph style={{ color: '#000' }}>Apakah anda ingin mencetak semua Order Packing Info ?</Paragraph>
				        </Dialog.Content>
				        <Dialog.Actions style={{
				        	justifyContent: 'space-between'
				        }}>
							<Button
								icon="printer"
								mode="outlined"
								style={{ borderColor: '#000' }}
								onPress={async () => {
									if (loadingPrintAll) {
										await abortController.abort();
										setLoadingPrintAll(false);
										setAbortConst(new AbortController());
									} else {
										printAllDataOrderPack();
									}
								}}
								loading={loadingPrintAll}
								labelStyle={{
									color: '#000'
								}}
							/>
							<Button icon="close" mode="outlined" onPress={hideDialogAlert} style={{ borderColor: '#000' }} labelStyle={{ color: '#000' }} />
						</Dialog.Actions>
			    	</Dialog>
			    </Portal>

			    <BarcodeForm
			    	visible={visibleBcForm}
			    	onDismiss={hiddenBcForm}
			    	title="Barcode Form"
			    	generatePacking={generatePacking}
			    	asyncData={asyncDataStorage}
					dtListOrder={dtListOrder}
					dtListBarang={dtListBarang}
					dtHistory={dtHistory}
					setDtListOrder={setDtListOrder}
					setDtListBarang={setDtListBarang}
					setDtHistory={setDtHistory}
			    />
			</SafeAreaView>

			<SnackbarAlert visible={visibleErrPrinter} onDismiss={dismissErrPrinter} duration={3000} backgroundColor="#000" textColor="#FFFFFF" message={ "Can't connect to printer, please check web server printer\n"+errPrinter } setActionOnPress={() => setVisibleErrPrinter(false)} />
			<SnackbarAlert visible={alertError} onDismiss={dismissAlertError} duration={3000} backgroundColor={colorAlert} textColor="#FFFFFF" message={dataAlertError} setActionOnPress={() => setAlertError(false)} />
		</View>
    );
}

export default PackingListScreen;
