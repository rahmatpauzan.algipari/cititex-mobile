import React from 'react';
import axios from 'axios';
import Config from './Config';
import {
	getExpiredToken
} from './Utils';
import AsyncStorage from '@react-native-community/async-storage';
import * as RootNavigation from '../navigators/RootNavigation.js';

const instance = axios.create({
	baseURL: Config.linkapi,
	timeout: 10000,
	headers: {
		'Content-Type': 'application/json',
		'Accept': 'application/json'
	}
});

// Add a request interceptor
instance.interceptors.request.use(
	async (config) => {
		const now = Date.now();
		const token = await AsyncStorage.getItem('@userToken');
		if (token !== null) {
			const lifeTime = getExpiredToken(token);
			if (now < lifeTime) {
				config.headers.authorization = 'Bearer '+token;
			}
		}
		return config;
	}, error => {
		// Do something with request error
		return Promise.reject(error);
	}
);

// Add a response interceptor
instance.interceptors.response.use((response) => {
	// Any status code that lie within the range of 2xx cause this function to trigger
	// Do something with response data
	return response;
}, function (error) {
	// Any status codes that falls outside the range of 2xx cause this function to trigger
	// Do something with response error
	if (error.code === 'ECONNABORTED') {
		console.log(error.code);
		return 'timeout';
    }

	return Promise.reject(error);
});

export default instance;
