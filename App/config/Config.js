const Config = {
	// linkapi: 'http://10.0.2.2:8080/', // for android virtual devices
	// linkapi: 'http://localhost:8080/', // for real devices
	linkapi: 'http://18.136.190.112/testing/apps/api/', // for local data api
	// linkapi: 'http://52.74.69.49/api/', // for access to data live
}

export default Config;