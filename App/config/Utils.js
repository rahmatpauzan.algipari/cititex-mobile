import React from 'react';
import jwt_decode from "jwt-decode";
import { useSelector } from 'react-redux';
import moment from "moment";
import AsyncStorage from '@react-native-community/async-storage';
import axios from './axiosConfig';

moment.locale('id');

export const getExpiredToken = (token) => {
	const decodeToken = jwt_decode(token);
	const lifeTime = decodeToken.exp*1000;

	return lifeTime;
}

export const convertToDateTime = (params) => {
	return moment(params).format('YYYY MM DD, HH:mm:ss');
}

const fetchDataSuccess = (json) => ({
	payload: json
});

const fetchDataFailure = (error) => ({
	payload: error
});

export const getDataLocation = async () => {
	const locationId = await AsyncStorage.getItem('@locationId');
	const arrayLocation = await AsyncStorage.getItem('@location');
	const parsingData = JSON.parse(arrayLocation);
	var dtArray;
	parsingData.map(arr => {
		if (locationId == arr.id) {
			dtArray = {
				'locationId': arr.id,
				'ipEpsonPrinter': arr.ipEpsonPrinter,
				'ipThermalPrinter': arr.ipThermalPrinter
			}
		}
	});

	return dtArray;
}

export const getRoles = async () => {
	const roles = await AsyncStorage.getItem('@roles');
	const dtroles = JSON.parse(roles);
	return dtroles;
}

export const getAxisCenterHeaderBarcode = (id) => {
	switch (id.length) {
		case 1:
			return 220;
		case 2:
			return 200;
		case 3:
			return 180;
		case 4:
			return 160;
		case 5:
			return 145;
		case 6:
			return 130;
		case 7:
			return 110;
		case 8:
			return 95;
		case 9:
			return 85;
		case 10:
			return 75;
		default:
			return 30;
	}
}

export const getAxisLocation = (id) => {
	switch (id.length) {
		case 1:
			return 332;
		case 2:
			return 315;
		case 3:
			return 300;
		case 4:
			return 280;
		case 5:
			return 268;
		case 6:
			return 250;
		case 7:
			return 235;
		case 8:
			return 218;
		case 9:
			return 202;
		case 10:
			return 185;
		default:
			return 50;
	}
}
