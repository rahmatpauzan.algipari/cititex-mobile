import React from 'react';
import { View, StyleSheet } from 'react-native';
import {
	Avatar,
	Title,
	Caption,
	Paragraph,
	Drawer,
	Text,
	TouchableRipple,
	Switch
} from 'react-native-paper';
import {
	DrawerContentScrollView,
	DrawerItem
} from '@react-navigation/drawer';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-community/async-storage';

import { useDispatch } from 'react-redux';
import { fetchLogout } from '../redux/actions';

export function DrawerNavigate(props) {
  const dispatch = useDispatch();
  const [user, setUser] = React.useState(null);

  AsyncStorage.getItem('@user').then(res => {
    if (res !== null) {
      setUser(res);
    }
  });

  const LogOut = async () => {
    props.navigation.closeDrawer();
    await dispatch(fetchLogout());
    props.navigation.navigate('Login');
  }

	return (
		<View style={{ flex: 1 }}>
			<DrawerContentScrollView {...props}>
				<View style={styles.drawerContent}>
					<View style={styles.userInfoSection}>
						<View style={{flexDirection:'row',marginTop: 15,backgroundColor: 'white'}}>
							<Avatar.Image 
                source={require('../../assets/images/user-image.png')}
                size={50}
                style={{
                  borderwidth: 3,
                  borderColor: 'black'
                }}
              />
              <View style={{marginLeft:15, flexDirection:'column'}}>
                <Title style={styles.title}>{ user ? user.toUpperCase() : 'Staff' }</Title>
                <Caption style={styles.caption}>Staff of Cititex</Caption>
              </View>
						</View>
					</View>

					<Drawer.Section style={styles.drawerSection}>
            <DrawerItem 
              icon={({color, size}) => (
                <Icon 
                  name="package-variant" 
                  color={color}
                  size={size}
                />
              )}
              label="Packing List"
              onPress={() => {props.navigation.navigate('PackingList')}}
            />
            <DrawerItem 
              icon={({color, size}) => (
                <Icon 
                  name="package-variant-closed" 
                  color={color}
                  size={size}
                />
              )}
              label="Pallet Info"
              onPress={() => {props.navigation.navigate('PalletInfo')}}
            />
            <DrawerItem 
              icon={({color, size}) => (
                <Icon 
                  name="transfer" 
                  color={color}
                  size={size}
                />
              )}
              label="Transfer Package"
              onPress={() => {props.navigation.navigate('TransferPackage')}}
            />
          </Drawer.Section>
				</View>
			</DrawerContentScrollView>
			<Drawer.Section style={styles.bottomDrawerSection}>
				<DrawerItem
					icon={({ color, size }) => (
						<Icon
						name="exit-to-app"
						color={color}
						size={size}
						/>
					)}
					label="Sign Out"
					onPress={() => LogOut()}
				/>
			</Drawer.Section>
		</View>
	);
}

const styles = StyleSheet.create({
    drawerContent: {
      flex: 1,
    },
    userInfoSection: {
      paddingLeft: 20,
      backgroundColor: 'white'
    },
    title: {
      fontSize: 16,
      marginTop: 3,
      fontWeight: 'bold',
      color: 'black',
    },
    caption: {
      fontSize: 14,
      lineHeight: 14,
      color: 'black',
    },
    row: {
      marginTop: 20,
      flexDirection: 'row',
      alignItems: 'center',
    },
    section: {
      flexDirection: 'row',
      alignItems: 'center',
      marginRight: 15,
    },
    paragraph: {
      fontWeight: 'bold',
      marginRight: 3,
    },
    drawerSection: {
      marginTop: 15,
    },
    bottomDrawerSection: {
        marginBottom: 15,
        borderTopColor: '#f4f4f4',
        borderTopWidth: 1
    },
    preference: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingVertical: 12,
      paddingHorizontal: 16,
    },
});