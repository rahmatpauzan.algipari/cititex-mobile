import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Icon from 'react-native-vector-icons/Ionicons';

import LocationScreen from '../screens/LocationScreen';
import HomeScreen from '../screens/HomeScreen';
import {
  Details,
  Profile
} from '../Screen';

const HomeStack = createStackNavigator();
const HomeStackScreen = ({ navigation }) => (
	<HomeStack.Navigator screenOptions={{
      headerStyle: {
        backgroundColor: '#191919',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold'
      }
    }}>
		<HomeStack.Screen
	        name="Home"
	        component={HomeScreen}
	        options={{
	          title: 'Dashboard',
	          headerLeft: () => (
	            <Icon.Button name="ios-menu" size={25} backgroundColor="#191919" onPress={() => navigation.openDrawer()}></Icon.Button>
	          )
	        }}
	    />
		<HomeStack.Screen
	        name="Details"
	        component={Details}
	    />
	</HomeStack.Navigator>
);

const LocationStack = createStackNavigator();
const LocationStackScreen = ({ navigation }) => (
	<LocationStack.Navigator headerMode="none">
		<LocationStack.Screen
			name="Location"
			component={LocationScreen}
		/>
	</LocationStack.Navigator>
);

const ProfileStack = createStackNavigator();
const ProfileStackScreen = ({ navigation }) => (
    <ProfileStack.Navigator headerMode="none">
      <ProfileStack.Screen name="Profile" component={Profile} />
    </ProfileStack.Navigator>
);

const Tab = createBottomTabNavigator();

const BottomTabNavigate = () => (
	<Tab.Navigator
		tabBarOptions={{
			activeTintColor: '#191919'
		}}
		initialRouteName="TabHome"
	>
		<Tab.Screen
			name="TabLocation"
			component={LocationStackScreen}
			options={{
				tabBarLabel: 'Location',
				tabBarIcon: ({ color }) => (
					<Icon name="location-outline" color={color} size={26} />
				),
	        }}
		/>
		<Tab.Screen
			name="TabHome"
			component={HomeStackScreen}
			options={{
				tabBarLabel: 'Home',
				tabBarIcon: ({color}) => (
					<Icon name="home-outline" color={color} size={26} />
				),
	        }}
		/>
		<Tab.Screen
        	name="TabProfile"
        	component={ProfileStackScreen}
        	options={{
        		tabBarLabel: 'Profile',
        		tabBarIcon: ({ color }) => (
            		<Icon name="person-outline" color={color} size={26} />
        		),
        	}}
      	/>
	</Tab.Navigator>
);

export default BottomTabNavigate;