import React from 'react';
import axios from '../config/axiosConfig';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { useSelector } from 'react-redux';

import {
  getExpiredToken,
  convertToDateTime
} from '../config/Utils';

import { DrawerNavigate } from './DrawerNavigate';
import BottomTabNavigate from './BottomTabNavigate';

import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/Ionicons';

import SplashScreen from '../screens/SplashScreen';
import LoginScreen from '../screens/LoginScreen';
import PackingListScreen from '../screens/PackingListScreen';
import PalletInfoScreen from '../screens/PalletInfoScreen';
import TransferPackageScreen from '../screens/transferPackage/TransferPackageScreen';
import CreateTransPackScreen from '../screens/transferPackage/CreateTransPackScreen';

const Drawer = createDrawerNavigator();
const DrawerScreen = ({ navigation }) => {
  return (
    <Drawer.Navigator drawerContent={props => <DrawerNavigate {...props}/>}>
      <Drawer.Screen
        name="Drawer"
        component={BottomTabNavigate}
      />
      <Drawer.Screen
        name="PackingList"
        component={PackingListScreen}
      />
      <Drawer.Screen
        name="PalletInfo"
        component={PalletInfoScreen}
      />
      <Drawer.Screen
        name="TransferPackage"
        component={TransferPackageScreen}
      />
      <Drawer.Screen
        name="CreateTransPack"
        component={CreateTransPackScreen}
      />
    </Drawer.Navigator>
  );
}

const RootStack = createStackNavigator();

const RootNavigate = () => {
  const [isLoading, setIsLoading] = React.useState(true);
  const [userToken, setUserToken] = React.useState(null);
  const [lifeToken, setLifeToken] = React.useState(null);
  const [isLogin, setIsLogin] = React.useState(false);

  React.useEffect(() => {
    check();
  }, []);

  const check = async () => {
    setTimeout(() => {
      setIsLoading(false);
    }, 3000);

    const userToken = await AsyncStorage.getItem('@userToken');
    if (userToken) {
      await setUserToken(userToken);
      const result = getExpiredToken(userToken);
      await setLifeToken(result);
    }

    const logged = await AsyncStorage.getItem('@isLogin');
    const isLogged = JSON.parse(logged);
    if (isLogged) {
      await setIsLogin(isLogged);
    }
  }

  if (isLoading) {
    return <SplashScreen />
  }
  return (
    <RootStack.Navigator
      initialRouteName={ lifeToken == null || isLogin == null ? 'Login' : isLogin === false ? 'Login' : (lifeToken < Date.now()) ? 'Login' : 'Home' }
      headerMode="none"
    >
      <RootStack.Screen name="Home" component={DrawerScreen} />
      <RootStack.Screen name="Login" component={LoginScreen} />
    </RootStack.Navigator>
  );
}

export default RootNavigate;
