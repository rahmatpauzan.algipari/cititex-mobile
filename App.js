import React from 'react';
import { Provider as StoreProvider } from 'react-redux';
import store from './App/redux/store/Store';
import RootNavigate from './App/navigators/RootNavigate';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { NavigationContainer } from '@react-navigation/native';
import { navigationRef } from './App/navigators/RootNavigation';

const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: '#3498db',
    accent: '#f1c40f',
  },
};

const App = () => {
  return (
    <StoreProvider store={store}>
    	<PaperProvider theme={theme}>
    		<NavigationContainer ref={navigationRef}>
          <RootNavigate />
        </NavigationContainer>
    	</PaperProvider>
    </StoreProvider>
  );
}

export default App;
